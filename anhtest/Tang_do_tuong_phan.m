clear all;
clc;
anhdicom = 'C:\Users\Duong Tuan Minh\Desktop\hoctap\hoctap-20191\HTTTYT\Dicom\Anonymized20181114.dcm';
A = dicomread(anhdicom);%lenh doc file anh y te
figure(1);
imshow(A,[]);%hien thi hinh anh thang do xam. Chia ty le hien thi tren pham vi gia tri pixel trong I.
title ('Anh ban dau');

Pb = mat2gray(A); % Chuyen sang anh xam
A_adjust = imadjust(Pb, [0.35 0.65], [0 1], 1);       % Dieu chinh do tuong phan
figure(2);
imshow(A_adjust); title('Anh chinh do tuong phan');