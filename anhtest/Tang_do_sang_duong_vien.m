clear all;
clc;
fname= 'C:\Users\Duong Tuan Minh\Desktop\hoctap\hoctap-20191\HTTTYT\Dicom\Anonymized20181114.dcm';
Readdicom= dicomread(fname);
info= dicominfo(fname);
%Readdicom= Readdicom;
figure(1);imshow(Readdicom,[]);title('Anh ban dau')

a= Readdicom;
[r,c]= size(a);
k= fspecial('gaussian',[3 3],1); %bo loc gauss
f= imfilter(a,k); %loc anh voi bo loc gauss
f2= a-f; %lay duong vien
m12= min(min(f2));
m2= max(max(f2));
mid1= abs(m12+m2)/2;
[r,c]= size(f2);
g1= 7;%he so nhan
g2= 0.4;
for i= 1:r
    for j= 1:c
        if abs(f2(i,j))< mid1
            f2(i,j)= g1*f2(i,j);
        else if abs(f2(i,j))>= mid1
                f2(i,j)= g2*f2(i,j);
            end
        end
    end
end
f3= f2;
figure(2);imshow(f2,[]);title('Anh sau khi tang do sang duong vien')