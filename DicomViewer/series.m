function varargout = series(varargin)
% SERIES MATLAB code for series.fig
%      SERIES, by itself, creates a new SERIES or raises the existing
%      singleton*.
%
%      H = SERIES returns the handle to a new SERIES or the handle to
%      the existing singleton*.
%
%      SERIES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SERIES.M with the given input arguments.
%
%      SERIES('Property','Value',...) creates a new SERIES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before series_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to series_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help series

% Last Modified by GUIDE v2.5 04-Jun-2020 15:25:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @series_OpeningFcn, ...
                   'gui_OutputFcn',  @series_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before series is made visible.
function series_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to series (see VARARGIN)
global info;
if isfield(info,'SeriesDate')set(handles.q13,'string',info.SeriesDate);else  set(handles.q13,'string','');end;
if isfield(info,'SeriesTime')set(handles.q14,'string',info.SeriesTime);else  set(handles.q14,'string','');end;
if isfield(info,'SeriesDescription')set(handles.q15,'string',info.SeriesDescription);else  set(handles.q15,'string','');end;
if isfield(info,'ProtocolName')set(handles.q16,'string',info.ProtocolName);else  set(handles.q16,'string','');end;
if isfield(info,'PatientPosition')set(handles.q17,'string',info.PatientPosition);else  set(handles.q17,'string','');end;
if isfield(info,'SeriesNumber')set(handles.q18,'string',info.SeriesNumber);else  set(handles.q18,'string','');end;
% Choose default command line output for series
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes series wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = series_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function q14_Callback(hObject, eventdata, handles)
% hObject    handle to q14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q14 as text
%        str2double(get(hObject,'String')) returns contents of q14 as a double


% --- Executes during object creation, after setting all properties.
function q14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q15_Callback(hObject, eventdata, handles)
% hObject    handle to q15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q15 as text
%        str2double(get(hObject,'String')) returns contents of q15 as a double


% --- Executes during object creation, after setting all properties.
function q15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q16_Callback(hObject, eventdata, handles)
% hObject    handle to q16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q16 as text
%        str2double(get(hObject,'String')) returns contents of q16 as a double


% --- Executes during object creation, after setting all properties.
function q16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q17_Callback(hObject, eventdata, handles)
% hObject    handle to q17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q17 as text
%        str2double(get(hObject,'String')) returns contents of q17 as a double


% --- Executes during object creation, after setting all properties.
function q17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q18_Callback(hObject, eventdata, handles)
% hObject    handle to q18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q18 as text
%        str2double(get(hObject,'String')) returns contents of q18 as a double


% --- Executes during object creation, after setting all properties.
function q18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q13_Callback(hObject, eventdata, handles)
% hObject    handle to q13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q13 as text
%        str2double(get(hObject,'String')) returns contents of q13 as a double


% --- Executes during object creation, after setting all properties.
function q13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
