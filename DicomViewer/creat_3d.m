function varargout = creat_3d(varargin)
% CREAT_3D MATLAB code for creat_3d.fig
%      CREAT_3D, by itself, creates a new CREAT_3D or raises the existing
%      singleton*.
%
%      H = CREAT_3D returns the handle to a new CREAT_3D or the handle to
%      the existing singleton*.
%
%      CREAT_3D('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREAT_3D.M with the given input arguments.
%
%      CREAT_3D('Property','Value',...) creates a new CREAT_3D or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before creat_3d_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to creat_3d_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help creat_3d

% Last Modified by GUIDE v2.5 16-May-2019 11:56:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @creat_3d_OpeningFcn, ...
                   'gui_OutputFcn',  @creat_3d_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before creat_3d is made visible.
function creat_3d_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to creat_3d (see VARARGIN)

% Choose default command line output for creat_3d
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% set(handles.slider1,'Value',50);
% UIWAIT makes creat_3d wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = creat_3d_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
global fileFolder I 

%% set image into axes_2
Selected = get(handles.listbox1, 'value');
numberOfFilesSelected = length(Selected);
% If more than one is selected, bail out.
if numberOfFilesSelected > 1
  return;
end
% If only one is selected, display it.
ListOfImageNames = get(handles.listbox1, 'string');
baseImageFileName = char(ListOfImageNames(Selected));
fullImageFileName = fullfile(fileFolder, baseImageFileName);% Prepend folder.
%Show number of slice
y=sprintf('Slice %d/%d ',Selected,length(ListOfImageNames));
set(handles.num,'string',y);
% Put into second listbox
I=uint16(dicomread(fullImageFileName));
axes(handles.axes_2);
montage(I);
set(gca,'clim',[0 100]);



% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in creat3d.
function creat3d_Callback(hObject, eventdata, handles)
% hObject    handle to creat3d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mri slider2 voxel_size h

im=mri(:,:,30);
max_level=double(max(im(:)));
%% Isolate and Display brain 
%Apply some thresholding rules to ignore certain parts of data
mriTemp=mri(:,:,30);
mriTemp(max_level:end,:)=0;
%% threshold on relaxation times
%Use morphological operations and blob analysis to exclude non-brain tissue
%Convert to black and white and dilate work on all the images
%Convert image to B/N and remove small blobs
lb=40;
ub=100;
mriAdjust=mri;
mriAdjust(mriAdjust<=lb)=0;
mriAdjust(mriAdjust>=ub)=0;
mriAdjust(slider2:end,:,:)=0;
bw=logical(mriAdjust);
%% let's clea that up a bit
nhood=ones([7 7 3]);
%morphologically open image
bw=imopen(bw,nhood);
%% Find blobs
L=bwlabeln(bw);
stats=regionprops(L,'Area','Centroid');
LL=L(:,:,30)+1;
cmap=hsv(length(stats));cmap=[0 0 0;cmap];
% LL=cmap(LL,:);
% LL=reshape(LL,[sizeI,3]);
%% Select Largest blob
%determine the largest blob and eliminate all other blobs
A=[stats.Area];
biggest=find(A == max(A));
mriAdjust(L~=biggest)=0;
%% Partition brain mass
%separate the brain mass into 2categories
level = thresh_tool(uint16(mriAdjust(:,:,30)),'gray');
mriBrainPartition=uint8(zeros(size(mriAdjust)));
mriBrainPartition(mriAdjust<level & mriAdjust>0)=2;
mriBrainPartition(mriAdjust>=level) =3;
%% Creat 3-D display
tmp=text(128,200,'Rendering 3-D: Please wait!','color','r','fontsize',14,...
  'horizontalalignment','center');
Ds=imresize(mriBrainPartition,0.25,'nearest');
Ds=flipud(Ds);
Ds=fliplr(Ds);
Ds=permute(Ds,[3 2 1]);
voxel_size2=voxel_size([1 3 2]).*[4 1 4];
white_vol=isosurface(Ds,2.5);
gray_vol=isosurface(Ds,1.5);

h=figure('visible','off','outerposition',[0 0 800 600],'renderer','openGL','WindowStyle','normal');
patch(white_vol,'FaceColor','b','EdgeColor','none');
patch(gray_vol,'FaceColor','y','EdgeColor','none','FaceAlpha',0.5);
view(45,15);daspect(1./voxel_size2);axis tight;axis off;
camlight;camlight(-80,-10);lighting phong;
delete(tmp);
movegui(h,'center');
set(h,'visible','on');

% --- Executes on button press in exit.
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end


function path_Callback(hObject, eventdata, handles)
% hObject    handle to path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of path as text
%        str2double(get(hObject,'String')) returns contents of path as a double


% --- Executes during object creation, after setting all properties.
function path_CreateFcn(hObject, eventdata, handles)
% hObject    handle to path (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in folder.
function folder_Callback(hObject, eventdata, handles)
% hObject    handle to folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fileFolder mri voxel_size 
%% Select file 
fileFolder=uigetdir('E:\matlab2015\bin','Select Folder');
files = dir(fullfile(fileFolder,'*.dcm'));
fileNames = {files.name};
set(handles.path,'string',fileFolder);
MyListOfFiles = [];
% Loop on every element in the folder and update the list
for i = 1:length(files)
   if files(i).isdir==0
       MyListOfFiles{end+1,1} = files(i).name;
   end
end
% update the listbox with the result
set(handles.listbox1,'String',MyListOfFiles)

%% Examine file header (metadata, from Dicom stack)
info = dicominfo(fullfile(fileFolder,fileNames{1}));

%extract size info from metadata
voxel_size= [info.PixelSpacing; info.SliceThickness]';
%Read one file to get size
I       = dicomread(fullfile(fileFolder,fileNames{1}));
classI  = class(I);
sizeI   = size(I);
numImages= length(fileNames);

name=info.PatientName;
name=struct2cell(name);
set(handles.name,'string',name);
set(handles.dob,'string',info.PatientBirthDate);
set(handles.seriesdate,'string',info.SeriesDate);
set(handles.seriestime,'string',info.SeriesTime);
set(handles.des,'string',info.StudyDescription);
set(handles.mod,'string',info.Modality);
%Read slice image; populate 3-D matrix
hWaitBar= waitbar(0,'Reading Dicom file');

%Creat array
mri = zeros(info.Rows,info.Columns, numImages,classI);

for i=length(fileNames):-1:1
    fname     =fullfile(fileFolder,fileNames{i});
    mri(:,:,i)=uint16(dicomread(fname));
    waitbar((length(fileNames)-i+1)/length(fileNames));
end

delete(hWaitBar)
%%Creat Montage
axes(handles.axes_1);
montage(reshape(uint16(mri),[size(mri,1),size(mri,2),1,size(mri,3)]),'DisplayRange',[]);
set(gca,'clim', [0 100]);
% drawnow;
% shg;
%% set image max into axes_3
im=mri(:,:,30);
max_level=double(max(im(:)));
axes(handles.axes_3);
imshow(im,[0,max_level]);

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global mri slider2
mriTemp=mri(:,:,30);
slider2=get(handles.slider2,'Value');
mriTemp(slider2:end,:)=0;
axes(handles.axes_3);
imshow(imadjust(mriTemp));

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global I mri
y=get(handles.slider1,'Value');
axes(handles.axes_1);
montage(reshape(uint16(mri),[size(mri,1),size(mri,2),1,size(mri,3)]),'DisplayRange',[]);
set(gca,'clim', [0 y]);
axes(handles.axes_2);
montage(I);
set(gca,'clim',[0 y]);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 global h
 filename=uiputfile('*.fig');
 if filename==0
     return
 else
 saveas(h,filename);
 end

% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function tool_Callback(hObject, eventdata, handles)
% hObject    handle to tool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
% hObject    handle to about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y=textread('About.txt', '%s', 'whitespace', '');
msgbox(y,'HomeWork 3');

% --------------------------------------------------------------------
function direction_Callback(hObject, eventdata, handles)
% hObject    handle to direction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y=textread('direction.txt', '%s', 'whitespace', '');
msgbox(y,'Direction Of Program');
% --------------------------------------------------------------------
function creat3d_i_Callback(hObject, eventdata, handles)
% hObject    handle to creat3d_i (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mri slider2 voxel_size h
im=mri(:,:,30);
max_level=double(max(im(:)));
%% Isolate and Display brain 
%Apply some thresholding rules to ignore certain parts of data
mriTemp=mri(:,:,30);
mriTemp(max_level:end,:)=0;
%% threshold on relaxation times
%Use morphological operations and blob analysis to exclude non-brain tissue
%Convert to black and white and dilate work on all the images
%Convert image to B/N and remove small blobs
lb=40;
ub=100;
mriAdjust=mri;
mriAdjust(mriAdjust<=lb)=0;
mriAdjust(mriAdjust>=ub)=0;
mriAdjust(slider2:end,:,:)=0;
bw=logical(mriAdjust);
%% let's clea that up a bit
nhood=ones([7 7 3]);
%morphologically open image
bw=imopen(bw,nhood);
%% Find blobs
L=bwlabeln(bw);
stats=regionprops(L,'Area','Centroid');
LL=L(:,:,30)+1;
cmap=hsv(length(stats));cmap=[0 0 0;cmap];
%determine the largest blob and eliminate all other blobs
A=[stats.Area];
biggest=find(A == max(A));
mriAdjust(L~=biggest)=0;
%% Partition brain mass
%separate the brain mass into 2categories
level = thresh_tool(uint16(mriAdjust(:,:,30)),'gray');
mriBrainPartition=uint8(zeros(size(mriAdjust)));
mriBrainPartition(mriAdjust<level & mriAdjust>0)=2;
mriBrainPartition(mriAdjust>=level) =3;
%% Creat 3-D display
tmp=text(128,200,'Rendering 3-D: Please wait!','color','r','fontsize',14,...
  'horizontalalignment','center');
Ds=imresize(mriBrainPartition,0.25,'nearest');
Ds=flipud(Ds);
Ds=fliplr(Ds);
Ds=permute(Ds,[3 2 1]);
voxel_size2=voxel_size([1 3 2]).*[4 1 4];
white_vol=isosurface(Ds,2.5);
gray_vol=isosurface(Ds,1.5);

h=figure('visible','off','outerposition',[0 0 800 600],'renderer','openGL','WindowStyle','normal');
patch(white_vol,'FaceColor','b','EdgeColor','none');
patch(gray_vol,'FaceColor','y','EdgeColor','none','FaceAlpha',0.5);
view(45,15);daspect(1./voxel_size2);axis tight;axis off;
camlight;camlight(-80,-10);lighting phong;
delete(tmp);
movegui(h,'center');
set(h,'visible','on');
% --------------------------------------------------------------------
function conv_Callback(hObject, eventdata, handles)
% hObject    handle to conv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function openf_Callback(hObject, eventdata, handles)
% hObject    handle to openf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global fileFolder mri voxel_size 
fileFolder=uigetdir('E:\matlab2015\bin','Select Folder');
files = dir(fullfile(fileFolder,'*.dcm'));
fileNames = {files.name};
set(handles.path,'string',fileFolder);
MyListOfFiles = [];
% Loop on every element in the folder and update the list
for i = 1:length(files)
   if files(i).isdir==0
       MyListOfFiles{end+1,1} = files(i).name;
   end
end
% update the listbox with the result
set(handles.listbox1,'String',MyListOfFiles)

%% Examine file header (metadata, from Dicom stack)
info = dicominfo(fullfile(fileFolder,fileNames{1}));

%extract size info from metadata
voxel_size= [info.PixelSpacing; info.SliceThickness]';
%Read one file to get size
I       = dicomread(fullfile(fileFolder,fileNames{1}));
classI  = class(I);
sizeI   = size(I);
numImages= length(fileNames);
name=info.PatientName;
name=struct2cell(name);
set(handles.name,'string',name);
set(handles.dob,'string',info.PatientBirthDate);
set(handles.seriesdate,'string',info.SeriesDate);
set(handles.seriestime,'string',info.SeriesTime);
set(handles.des,'string',info.StudyDescription);
set(handles.mod,'string',info.Modality);
% Read slice image; populate 3-D matrix
hWaitBar= waitbar(0,'Reading Dicom file');

%Creat array
mri = zeros(info.Rows,info.Columns, numImages,classI);

for i=length(fileNames):-1:1
    fname     =fullfile(fileFolder,fileNames{i});
    mri(:,:,i)=uint16(dicomread(fname));
    waitbar((length(fileNames)-i+1)/length(fileNames));
end
delete(hWaitBar)
%%Creat Montage
axes(handles.axes_1);
montage(reshape(uint16(mri),[size(mri,1),size(mri,2),1,size(mri,3)]),'DisplayRange',[]);
set(gca,'clim', [0 100]);
% drawnow;
% shg;
%% set image max into axes_3
im=mri(:,:,30);
max_level=double(max(im(:)));
axes(handles.axes_3);
imshow(im,[0,max_level]);
% --------------------------------------------------------------------
function saveas_Callback(hObject, eventdata, handles)
% hObject    handle to saveas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h
 filename=uiputfile('*.fig');
 if filename==0
     return
 else
 saveas(h,filename);
 end
% --------------------------------------------------------------------
function exitp_Callback(hObject, eventdata, handles)
% hObject    handle to exitp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end

% --------------------------------------------------------------------
function jpg_Callback(hObject, eventdata, handles)
% hObject    handle to jpg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
k=uint8(255 * mat2gray(I));
myicon=imread('ok_1.jpg');
[filename path]=uiputfile();
imwrite(k,sprintf('%s.jpg',strrep(filename, '.rpt','')), 'jpg');%strrep in order to replace string a by b
msgbox('Finished saving .jpg image','Complete','custom',myicon);

% --------------------------------------------------------------------
function bmp_Callback(hObject, eventdata, handles)
% hObject    handle to bmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
k=uint8(255 * mat2gray(I));
myicon=imread('ok_1.jpg');
[filename path]=uiputfile();
imwrite(k,sprintf('%s.bmp',strrep(filename, '.rpt','')), 'bmp');%strrep in order to replace string a by b
msgbox('Finished saving .bmp image','Complete','custom',myicon);


% --------------------------------------------------------------------
function tiff_Callback(hObject, eventdata, handles)
% hObject    handle to tiff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
k=uint8(255 * mat2gray(I));
myicon=imread('ok_1.jpg');
[filename path]=uiputfile();
imwrite(k,sprintf('%s.tiff',strrep(filename, '.rpt','')), 'tiff');%strrep in order to replace string a by b
msgbox('Finished saving .tiff image','Complete','custom',myicon);


% --------------------------------------------------------------------
function gif_Callback(hObject, eventdata, handles)
% hObject    handle to gif (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
k=uint8(255 * mat2gray(I));
myicon=imread('ok_1.jpg');
[filename path]=uiputfile();
imwrite(k,sprintf('%s.gif',strrep(filename, '.rpt','')), 'gif');%strrep in order to replace string a by b
msgbox('Finished saving .gif image','Complete','custom',myicon);


% --------------------------------------------------------------------
function png_Callback(hObject, eventdata, handles)
% hObject    handle to png (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
k=uint8(255 * mat2gray(I));
myicon=imread('ok_1.jpg');
[filename path]=uiputfile();
imwrite(k,sprintf('%s.png',strrep(filename, '.rpt','')), 'png');%strrep in order to replace string a by b
msgbox('Finished saving .png image','Complete','custom',myicon);


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global I mri
y=get(handles.slider1,'Value');
axes(handles.axes_2);
imshow(I);
set(gca,'clim',[0 y]);

h = imdistline(gca);
api = iptgetapi(h);
fcn = makeConstrainToRectFcn('imline',...
                              get(gca,'XLim'),get(gca,'YLim'));
api.setDragConstraintFcn(fcn); 
 

% --- Executes during object creation, after setting all properties.
function axes_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_2


% --------------------------------------------------------------------
function view_Callback(hObject, eventdata, handles)
% hObject    handle to view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function zoom_Callback(hObject, eventdata, handles)
% hObject    handle to zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom on; 

% --------------------------------------------------------------------
function mesuareim_Callback(hObject,eventdata, handles)
global I mri
y=get(handles.slider1,'Value');
axes(handles.axes_2);
imshow(I);
set(gca,'clim',[0 y]); 
h = imdistline(gca);
api = iptgetapi(h);
fcn = makeConstrainToRectFcn('imline',...
                              get(gca,'XLim'),get(gca,'YLim'));
api.setDragConstraintFcn(fcn); 
 
