function varargout = StudyModule(varargin)
% STUDYMODULE MATLAB code for StudyModule.fig
%      STUDYMODULE, by itself, creates a new STUDYMODULE or raises the existing
%      singleton*.
%
%      H = STUDYMODULE returns the handle to a new STUDYMODULE or the handle to
%      the existing singleton*.
%
%      STUDYMODULE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STUDYMODULE.M with the given input arguments.
%
%      STUDYMODULE('Property','Value',...) creates a new STUDYMODULE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before StudyModule_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to StudyModule_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help StudyModule

% Last Modified by GUIDE v2.5 02-Jul-2020 02:08:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @StudyModule_OpeningFcn, ...
                   'gui_OutputFcn',  @StudyModule_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before StudyModule is made visible.
function StudyModule_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to StudyModule (see VARARGIN)
global info;
if isfield(info,'StudyDate')set(handles.q8,'string',info.StudyDate);else  set(handles.q8,'string',''); end;
if isfield(info,'StudyTime')set(handles.q9,'string',info.StudyTime);else  set(handles.q9,'string','');end;
if isfield(info,'StudyID')set(handles.q10,'string',info.StudyID);else  set(handles.q10,'string','');end;
if isfield(info,'Modality')set(handles.q11,'string',info.Modality);else  set(handles.q11,'string',''); end;
if isfield(info,'StudyDescription')set(handles.q12,'string',info.StudyDescription);else  set(handles.q12,'string','');end;
% Choose default command line output for StudyModule
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes StudyModule wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = StudyModule_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function q8_Callback(hObject, eventdata, handles)
% hObject    handle to q8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q8 as text
%        str2double(get(hObject,'String')) returns contents of q8 as a double


% --- Executes during object creation, after setting all properties.
function q8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q9_Callback(hObject, eventdata, handles)
% hObject    handle to q9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q9 as text
%        str2double(get(hObject,'String')) returns contents of q9 as a double


% --- Executes during object creation, after setting all properties.
function q9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q12_Callback(hObject, eventdata, handles)
% hObject    handle to q12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q12 as text
%        str2double(get(hObject,'String')) returns contents of q12 as a double


% --- Executes during object creation, after setting all properties.
function q12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q11_Callback(hObject, eventdata, handles)
% hObject    handle to q11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q11 as text
%        str2double(get(hObject,'String')) returns contents of q11 as a double


% --- Executes during object creation, after setting all properties.
function q11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q10_Callback(hObject, eventdata, handles)
% hObject    handle to q10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q10 as text
%        str2double(get(hObject,'String')) returns contents of q10 as a double


% --- Executes during object creation, after setting all properties.
function q10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
