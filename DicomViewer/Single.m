function varargout = Single(varargin)
% SINGLE MATLAB code for Single.fig
%      SINGLE, by itself, creates a new SINGLE or raises the existing
%      singleton*.
%
%      H = SINGLE returns the handle to a new SINGLE or the handle to
%      the existing singleton*.
%
%      SINGLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SINGLE.M with the given input arguments.
%
%      SINGLE('Property','Value',...) creates a new SINGLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Single_OpeningFcn gets called.  An
%      unrecognized property name or inv5alid value makes property application
%      stop.  All inputs are passed to Single_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Single

% Last Modified by GUIDE v2.5 02-Jul-2020 03:43:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Single_OpeningFcn, ...
                   'gui_OutputFcn',  @Single_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Single is made visible.
function Single_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Single (see VARARGIN)

set(handles.pushbutton5,'Visible','off');
set(handles.pushbutton6,'Visible','off');
set(handles.pushbutton8,'enable','off');
set(handles.pushbutton9,'enable','off');
set(handles.text45,'Visible','off');
set(handles.text46,'Visible','off');
set(handles.num,'Visible','off');
set(handles.listbox2,'enable','off');
axes(handles.axes19);
next=imread('3D.jpg');
imshow(next,[]);


axes(handles.axes20);
next=imread('logoappdemo.jpg');
imshow(next,[]);

% Choose default command line output for Single
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Single wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Single_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
axes(handles.axes6);
  global file;
  imshow(dicomread(file),[]);
  demon=dicomread(file)+29600+3100*get(handles.slider5,'Value');
  
 imshow(demon,[]);
  
  %global imageOpened;
%global newimg;
%global in;
%global tt;
%global file;
%if(file~=0)
%Readdicom=imageOpened;a= Readdicom;
%[r,c]= size(a);
%k= fspecial('gaussian',[3 3],1); %bo loc gauss
%f= imfilter(a,k); %loc anh voi bo loc gauss
%f2= a-f; %lay duong vien
%m12= min(min(f2));
%m2= max(max(f2));
%mid1= abs(m12+m2)/2;
%[r,c]= size(f2);
%g1= 7*get(handles.slider5,'Value');%he so nhan
%g2= 0.4;
%for i= 1:r
   % for j= 1:c
        %if abs(f2(i,j))< mid1
          %  f2(i,j)= g1*f2(i,j);
     %   else if abs(f2(i,j))>= mid1
           %     f2(i,j)= g2*f2(i,j);
         %   end
        %end
    %end
%end
%f3= f2;
%newimg=f2;
% axes(handles.axes6);imshow(f2,[]);
 
 
 %end;
  

% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider6_Callback(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.axes6);
global file;
  imshow(dicomread(file),[]);
y=get(handles.slider6,'Value');
handles.x=6000-5999*y ;
guidata(hObject, handles);

set(gca,'clim',[0 handles.x]);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
        

% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global open;
global folder_name I 
global j;
global fileNames;
global ListOfImageNames;
global Selected;
%% set image into axes_2
if(open==2)
Selected = get(handles.listbox2, 'value');
numberOfFilesSelected = length(Selected);
% If more than one is selected, bail out.
if numberOfFilesSelected > 1
  return;
end
% If only one is selected, display it.
ListOfImageNames = get(handles.listbox2, 'string');
baseImageFileName = char(ListOfImageNames(Selected));
fullImageFileName = fullfile(folder_name, baseImageFileName);% Prepend folder.
%Show number of slice
y=sprintf('Slice %d/%d ',Selected,length(ListOfImageNames));
set(handles.num,'string',y);
% Put into second listbox
I=uint16(dicomread(fullImageFileName));
axes(handles.axes6);
imshow(dicomread(fullfile(folder_name,fileNames{j})),[]);
%montage(I);
%set(gca,'clim',[0 50]);



numImages= length(fileNames);
j=Selected;
if(j>4 & j< numImages)
axes(handles.axes9);
demo=dicomread(fullfile(folder_name,fileNames{j-4}));
imshow(demo,[]);
else
    axes(handles.axes9);
    imshow(0.94,[]);
end;

if(j>3 & j< numImages)
axes(handles.axes10);
demo=dicomread(fullfile(folder_name,fileNames{j-3}));
imshow(demo,[]);else
    axes(handles.axes10);
    imshow(0.94,[]);
end;

if(j>2 & j< numImages)
axes(handles.axes11);
demo=dicomread(fullfile(folder_name,fileNames{j-2}));
imshow(demo,[]);else
    axes(handles.axe11);
    imshow(0.94,[]);
end;

if(j>1 & j< numImages)
axes(handles.axes12);
demo=dicomread(fullfile(folder_name,fileNames{j-1}));
imshow(demo,[]);else
    axes(handles.axes12);
    imshow(0.94,[]);
end;

if(j>-1 & j< numImages-1)
axes(handles.axes13);
demo=dicomread(fullfile(folder_name,fileNames{j+1}));
imshow(demo,[]);else
    axes(handles.axes13);
    imshow(0.94,[]);
end;

if(j>-2 & j< numImages-2)
axes(handles.axes14);
demo=dicomread(fullfile(folder_name,fileNames{j+2}));
imshow(demo,[]);else
    axes(handles.axes14);
    imshow(0.94,[]);
end;

if(j>-3 & j< numImages-3)
axes(handles.axes15);
demo=dicomread(fullfile(folder_name,fileNames{j+3}));
imshow(demo,[]);else
    axes(handles.axes15);
    imshow(0.94,[]);
end;

if(j>-4 & j< numImages-4)
axes(handles.axes16);
demo=dicomread(fullfile(folder_name,fileNames{j+4}));
imshow(demo,[]);else
    axes(handles.axes16);
    imshow(0.94,[]);
end;

if(j>0 & j< numImages)
axes(handles.axes17);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;

info = dicominfo(fullfile(folder_name,fileNames{get(handles.listbox2, 'value')}));
if isfield(info,'PatientName')set(handles.q1,'string',info.PatientName.FamilyName);else  set(handles.q1,'string','');end;
if isfield(info,'PatientID')set(handles.q2,'string',info.PatientID); else  set(handles.q2,'string','');end;
if isfield(info,'PatientSex')set(handles.q3,'string',info.PatientSex);else  set(handles.q3,'string','');end;
if isfield(info,'PatientBirthDate')set(handles.q4,'string',info.PatientBirthDate);else  set(handles.q4,'string','');end;
if isfield(info,'PatientWeight')set(handles.q5,'string',info.PatientWeight);else  set(handles.q5,'string','');end;    
if isfield(info,'PatientAge')set(handles.q6,'string',info.PatientAge);else  set(handles.q6,'string',''); end; 
if isfield(info,'PatientAddress')set(handles.q7,'string',info.PatientAddress);else  set(handles.q7,'string','');end;
       
if isfield(info,'StudyDate')set(handles.q8,'string',info.StudyDate);else  set(handles.q8,'string',''); end;
if isfield(info,'StudyTime')set(handles.q9,'string',info.StudyTime);else  set(handles.q9,'string','');end;
if isfield(info,'StudyID')set(handles.q10,'string',info.StudyID);else  set(handles.q10,'string','');end;
if isfield(info,'Modality')set(handles.q11,'string',info.Modality);else  set(handles.q11,'string',''); end;
if isfield(info,'StudyDescription')set(handles.q12,'string',info.StudyDescription);else  set(handles.q12,'string','');end;

if isfield(info,'SeriesDate')set(handles.q13,'string',info.SeriesDate);else  set(handles.q13,'string','');end;
if isfield(info,'SeriesTime')set(handles.q14,'string',info.SeriesTime);else  set(handles.q14,'string','');end;
if isfield(info,'SeriesDescription')set(handles.q15,'string',info.SeriesDescription);else  set(handles.q15,'string','');end;
if isfield(info,'ProtocolName')set(handles.q16,'string',info.ProtocolName);else  set(handles.q16,'string','');end;
if isfield(info,'PatientPosition')set(handles.q17,'string',info.PatientPosition);else  set(handles.q17,'string','');end;
if isfield(info,'SeriesNumber')set(handles.q18,'string',info.SeriesNumber);else  set(handles.q18,'string','');end;
% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2

end;
% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global imageOpened;
global open;
set(handles.slider5,'Value',0);
set(handles.slider6,'Value',0);
global j;
global folder_name;
global fileNames;
if(open==1)
    imshow(imageOpened,[]);
    set(handles.slider5,'Value',0);
set(handles.slider6,'Value',0);
axes(handles.axes6);
zoom off;

end;

if(open==2)

axes(handles.axes6);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
    set(handles.slider5,'Value',0);
set(handles.slider6,'Value',0);
axes(handles.axes6);
zoom off;
end;

% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function view_Callback(hObject, eventdata, handles)
% hObject    handle to view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function edit_Callback(hObject, eventdata, handles)



% --------------------------------------------------------------------
function export_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function tool_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function close_Callback(hObject, eventdata, handles)
global j;
j=1;
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end

% --------------------------------------------------------------------

function meassure_Callback(hObject, eventdata, handles)
axes(handles.axes6);
h = imdistline(gca);
api = iptgetapi(h);
fcn = makeConstrainToRectFcn('imline',...
get(gca,'XLim'),get(gca,'YLim'));
api.setDragConstraintFcn(fcn); 

% --------------------------------------------------------------------
function Create3D_Callback(hObject, eventdata, handles)
creat_3d;

% --------------------------------------------------------------------
function excel_Callback(hObject, eventdata, handles)
writetable(struct2table(handles.info_dicom, 'AsArray', true), 'Detail.xlsx');
myicon=imread('ok.jpg');
infosave  = fullfile(pwd,'Detail.xlsx');
msgbox({'Save Complete to:',infosave,'    Openning'},'Save info','custom',myicon);
open Detail.xlsx;


% --------------------------------------------------------------------
function word_Callback(hObject, eventdata, handles)
global imageOpened;
global chuky;
chuky=0;
xquang;
% --------------------------------------------------------------------
function cont
rast_Callback(hObject, eventdata, handles)
global y;
y=get(handles.slider6,'Value');
Edit

% --------------------------------------------------------------------
function Brightness_Callback(hObject, eventdata, handles)
Edit

% --------------------------------------------------------------------
function Segmentation_Callback(hObject, eventdata, handles)
Edit

% --------------------------------------------------------------------
function open_Callback(hObject, eventdata, handles)
global imageOpened;
global info_dicom;
global info;
global open;
global file;
global folder_name;
global open;
set(handles.pushbutton5,'Visible','off');
set(handles.pushbutton6,'Visible','off');
set(handles.pushbutton8,'enable','off');
set(handles.pushbutton9,'enable','off');
set(handles.text45,'Visible','off');
set(handles.text46,'Visible','off');
set(handles.num,'Visible','off');
set(handles.listbox2,'enable','off');
[file1 path] = uigetfile({'*.dcm'},'File Selector');
handles.filedcm = strcat(path, file1);
file=handles.filedcm;
guidata(hObject, handles);
folder_name=[file,path];
   axes(handles.axes6);
   if(handles.filedcm~=0)
 img = dicomread(handles.filedcm);
 imageOpened=img;
 imshow(img,[]);
guidata(hObject,handles);

end;
if(handles.filedcm~=0)
info=dicominfo(handles.filedcm);
 %handles.info_dicom=info;
     %guidata(hObject,handles);
end;
if isfield(info,'PatientName')set(handles.q1,'string',info.PatientName.FamilyName);else  set(handles.q1,'string','');end;
if isfield(info,'PatientID')set(handles.q2,'string',info.PatientID); else  set(handles.q2,'string','');end;
if isfield(info,'PatientSex')set(handles.q3,'string',info.PatientSex);else  set(handles.q3,'string','');end;
if isfield(info,'PatientBirthDate')set(handles.q4,'string',info.PatientBirthDate);else  set(handles.q4,'string','');end;
if isfield(info,'PatientWeight')set(handles.q5,'string',info.PatientWeight);else  set(handles.q5,'string','');end;    
if isfield(info,'PatientAge')set(handles.q6,'string',info.PatientAge);else  set(handles.q6,'string',''); end; 
if isfield(info,'PatientAddress')set(handles.q7,'string',info.PatientAddress);else  set(handles.q7,'string','');end;
       
if isfield(info,'StudyDate')set(handles.q8,'string',info.StudyDate);else  set(handles.q8,'string',''); end;
if isfield(info,'StudyTime')set(handles.q9,'string',info.StudyTime);else  set(handles.q9,'string','');end;
if isfield(info,'StudyID')set(handles.q10,'string',info.StudyID);else  set(handles.q10,'string','');end;
if isfield(info,'Modality')set(handles.q11,'string',info.Modality);else  set(handles.q11,'string',''); end;
if isfield(info,'StudyDescription')set(handles.q12,'string',info.StudyDescription);else  set(handles.q12,'string','');end;

if isfield(info,'SeriesDate')set(handles.q13,'string',info.SeriesDate);else  set(handles.q13,'string','');end;
if isfield(info,'SeriesTime')set(handles.q14,'string',info.SeriesTime);else  set(handles.q14,'string','');end;
if isfield(info,'SeriesDescription')set(handles.q15,'string',info.SeriesDescription);else  set(handles.q15,'string','');end;
if isfield(info,'ProtocolName')set(handles.q16,'string',info.ProtocolName);else  set(handles.q16,'string','');end;
if isfield(info,'PatientPosition')set(handles.q17,'string',info.PatientPosition);else  set(handles.q17,'string','');end;
if isfield(info,'SeriesNumber')set(handles.q18,'string',info.SeriesNumber);else  set(handles.q18,'string','');end;
if (open==2)
    axes(handles.axes17);
     imshow(0.94,[]);
     axes(handles.axes9);
     imshow(0.94,[]);
     axes(handles.axes10);
     imshow(0.94,[]);
     axes(handles.axes11);
     imshow(0.94,[]);
     axes(handles.axes12);
     imshow(0.94,[]);
     axes(handles.axes13);
     imshow(0.94,[]);
     axes(handles.axes14);
     imshow(0.94,[]);
     axes(handles.axes15);
     imshow(0.94,[]);
     axes(handles.axes16);
     imshow(0.94,[]);
     
end;open=1; 
   
%-------------------------------
function Openfolder_Callback(hObject, eventdata, handles)
global fileFolder mri voxel_size 
global j;
global folder_name;
global fileNames;
global open;
open=2;
j=1;
% LIST FILE NAME
folder_name = uigetdir;
files = dir(fullfile(folder_name,'*.dcm'));
fileNames = {files.name};
numImages= length(fileNames);
hWaitBar= waitbar(0,'Reading Dicom file');
%set(handles.path,'string',fileFolder);
MyListOfFiles = [];



% Loop on every element in the folder and update the list
for i = 1:length(files)
   if files(i).isdir==0
       MyListOfFiles{end+1,1} = files(i).name;
   end
end




% update the listbox with the result
set(handles.listbox2,'String',MyListOfFiles)

info = dicominfo(fullfile(folder_name,fileNames{j}));
 handles.info_dicom=info;
 voxel_size= [info.PixelSpacing; info.SliceThickness]';
%WaitBar

I       = dicomread(fullfile(folder_name,fileNames{j}));
classI  = class(I);
sizeI   = size(I);
mri = zeros(info.Rows,info.Columns, numImages,classI);


for i=length(fileNames):-1:1
    fname     =fullfile(folder_name,fileNames{i});
    mri(:,:,i)=uint16(dicomread(fname));
    waitbar((length(fileNames)-i+1)/length(fileNames));
end

delete(hWaitBar);




     guidata(hObject,handles);
if isfield(info,'PatientName')set(handles.q1,'string',info.PatientName.FamilyName);else  set(handles.q1,'string','');end;
if isfield(info,'PatientID')set(handles.q2,'string',info.PatientID); else  set(handles.q2,'string','');end;
if isfield(info,'PatientSex')set(handles.q3,'string',info.PatientSex);else  set(handles.q3,'string','');end;
if isfield(info,'PatientBirthDate')set(handles.q4,'string',info.PatientBirthDate);else  set(handles.q4,'string','');end;
if isfield(info,'PatientWeight')set(handles.q5,'string',info.PatientWeight);else  set(handles.q5,'string','');end;    
if isfield(info,'PatientAge')set(handles.q6,'string',info.PatientAge);else  set(handles.q6,'string',''); end; 
if isfield(info,'PatientAddress')set(handles.q7,'string',info.PatientAddress);else  set(handles.q7,'string','');end;
       
if isfield(info,'StudyDate')set(handles.q8,'string',info.StudyDate);else  set(handles.q8,'string',''); end;
if isfield(info,'StudyTime')set(handles.q9,'string',info.StudyTime);else  set(handles.q9,'string','');end;
if isfield(info,'StudyID')set(handles.q10,'string',info.StudyID);else  set(handles.q10,'string','');end;
if isfield(info,'Modality')set(handles.q11,'string',info.Modality);else  set(handles.q11,'string',''); end;
if isfield(info,'StudyDescription')set(handles.q12,'string',info.StudyDescription);else  set(handles.q12,'string','');end;

if isfield(info,'SeriesDate')set(handles.q13,'string',info.SeriesDate);else  set(handles.q13,'string','');end;
if isfield(info,'SeriesTime')set(handles.q14,'string',info.SeriesTime);else  set(handles.q14,'string','');end;
if isfield(info,'SeriesDescription')set(handles.q15,'string',info.SeriesDescription);else  set(handles.q15,'string','');end;
if isfield(info,'ProtocolName')set(handles.q16,'string',info.ProtocolName);else  set(handles.q16,'string','');end;
if isfield(info,'PatientPosition')set(handles.q17,'string',info.PatientPosition);else  set(handles.q17,'string','');end;
if isfield(info,'SeriesNumber')set(handles.q18,'string',info.SeriesNumber);else  set(handles.q18,'string','');end;

%extract size info from metadata
%voxel_size= [info.PixelSpacing; info.SliceThickness]';
%Read one file to get size
axes(handles.axes19);
montage(reshape(uint16(mri),[size(mri,1),size(mri,2),1,size(mri,3)]),'DisplayRange',[]);
set(gca,'clim', [0 100]);
%DISPLAY IMAGES


if(j>0 & j< numImages)
axes(handles.axes6);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;

if(j>4 & j< numImages)
axes(handles.axes9);
demo=dicomread(fullfile(folder_name,fileNames{j-4}));
imshow(demo,[]);
end;

if(j>3 & j< numImages)
axes(handles.axes10);
demo=dicomread(fullfile(folder_name,fileNames{j-3}));
imshow(demo,[]);
end;

if(j>2 & j< numImages)
axes(handles.axes11);
demo=dicomread(fullfile(folder_name,fileNames{j-2}));
imshow(demo,[]);
end;

if(j>1 & j< numImages)
axes(handles.axes12);
demo=dicomread(fullfile(folder_name,fileNames{j-1}));
imshow(demo,[]);
end;

if(j>-1 & j< numImages)
axes(handles.axes13);
demo=dicomread(fullfile(folder_name,fileNames{j+1}));
imshow(demo,[]);
end;

if(j>-2 & j< numImages)
axes(handles.axes14);
demo=dicomread(fullfile(folder_name,fileNames{j+2}));
imshow(demo,[]);
end;

if(j>-3 & j< numImages)
axes(handles.axes15);
demo=dicomread(fullfile(folder_name,fileNames{j+3}));
imshow(demo,[]);
end;

if(j>-4 & j< numImages)
axes(handles.axes16);
demo=dicomread(fullfile(folder_name,fileNames{j+4}));
imshow(demo,[]);
end;

if(j>0 & j< numImages)
axes(handles.axes17);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;
set(handles.pushbutton5,'Visible','on');
set(handles.pushbutton6,'Visible','on');
set(handles.pushbutton8,'enable','on');
set(handles.pushbutton9,'enable','on');
set(handles.text45,'Visible','on');
set(handles.text46,'Visible','on');
set(handles.num,'Visible','on');
set(handles.listbox2,'enable','on');
set(handles.listbox2, 'value', 1);
% --------------------------------------------------------------------
function saveas_Callback(hObject, eventdata, handles)
% hObject    handle to saveas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global imageOpened;
 [newname newfile]=uiputfile({'*.jpg','JPEG Files(*.jpg)';...
     '*.bmp','Bitmap Files(*.bmp)'; '*.tif','TIFF Files(*.tif)';...
     '*.png','PNG Files(*.png)'; '*.gif','GIF Files(*.gif)';...
    '*.*','all image file'},'Save','anhkq/');
if([newname newfile]~=0)
 imageOpened=uint8(255 * mat2gray(imageOpened));
 imwrite(imageOpened,[newfile,newname]);
 msgbox('Anh da duoc luu');end;

% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global j;
j=1;
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end

% --------------------------------------------------------------------
function patient_Callback(hObject, eventdata, handles)
% hObject    handle to patient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
patient;

% --------------------------------------------------------------------
function study_Callback(hObject, eventdata, handles)
% hObject    handle to study (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
StudyModule;

% --------------------------------------------------------------------
function series_Callback(hObject, eventdata, handles)
% hObject    handle to series (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
series

% --------------------------------------------------------------------
function filemetainfo_Callback(hObject, eventdata, handles)
% hObject    handle to filemetainfo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FileMetaInfo

% --------------------------------------------------------------------
function Frameofreference_Callback(hObject, eventdata, handles)
% hObject    handle to Frameofreference (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function image_Callback(hObject, eventdata, handles)
% hObject    handle to image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imagebasic;

% --------------------------------------------------------------------
function imtool_Callback(hObject, eventdata, handles)
% hObject    handle to imtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global imageOpened;
imtool(imageOpened,'DisplayRange',[]);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be d
efined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q3_Callback(hObject, eventdata, handles)
% hObject    handle to q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q3 as text
%        str2double(get(hObject,'String')) returns contents of q3 as a double


% --- Executes during object creation, after setting all properties.
function q3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q4_Callback(hObject, eventdata, handles)
% hObject    handle to q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q3 as text
%        str2double(get(hObject,'String')) returns contents of q3 as a double


% --- Executes during object creation, after setting all properties.
function q4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q5_Callback(hObject, eventdata, handles)
% hObject    handle to q5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q5 as text
%        str2double(get(hObject,'String')) returns contents of q5 as a double


% --- Executes during object creation, after setting all properties.
function q5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q6_Callback(hObject, eventdata, handles)
% hObject    handle to q5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q5 as text
%        str2double(get(hObject,'String')) returns contents of q5 as a double


% --- Executes during object creation, after setting all properties.
function q6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q7_Callback(hObject, eventdata, handles)
% hObject    handle to q7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q7 as text
%        str2double(get(hObject,'String')) returns contents of q7 as a double


% --- Executes during object creation, after setting all properties.
function q7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q13_Callback(hObject, eventdata, handles)
% hObject    handle to q13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q13 as text
%        str2double(get(hObject,'String')) returns contents of q13 as a double


% --- Executes during object creation, after setting all properties.
function q13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q14_Callback(hObject, eventdata, handles)
% hObject    handle to q14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q14 as text
%        str2double(get(hObject,'String')) returns contents of q14 as a double


% --- Executes during object creation, after setting all properties.
function q14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q15_Callback(hObject, eventdata, handles)
% hObject    handle to q15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q15 as text
%        str2double(get(hObject,'String')) returns contents of q15 as a double


% --- Executes during object creation, after setting all properties.
function q15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q8_Callback(hObject, eventdata, handles)
% hObject    handle to q8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q8 as text
%        str2double(get(hObject,'String')) returns contents of q8 as a double


% --- Executes during object creation, after setting all properties.
function q8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q9_Callback(hObject, eventdata, handles)
% hObject    handle to q9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q9 as text
%        str2double(get(hObject,'String')) returns contents of q9 as a double


% --- Executes during object creation, after setting all properties.
function q9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q12_Callback(hObject, eventdata, handles)
% hObject    handle to q12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q12 as text
%        str2double(get(hObject,'String')) returns contents of q12 as a double


% --- Executes during object creation, after setting all properties.
function q12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q11_Callback(hObject, eventdata, handles)
% hObject    handle to q11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q11 as text
%        str2double(get(hObject,'String')) returns contents of q11 as a double


% --- Executes during object creation, after setting all properties.
function q11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q10_Callback(hObject, eventdata, handles)
% hObject    handle to q10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q10 as text
%        str2double(get(hObject,'String')) returns contents of q10 as a double


% --- Executes during object creation, after setting all properties.
function q10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q16_Callback(hObject, eventdata, handles)
% hObject    handle to q16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q16 as text
%        str2double(get(hObject,'String')) returns contents of q16 as a double


% --- Executes during object creation, after setting all properties.
function q16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q17_Callback(hObject, eventdata, handles)
% hObject    handle to q17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q17 as text
%        str2double(get(hObject,'String')) returns contents of q17 as a double


% --- Executes during object creation, after setting all properties.
function q17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q18_Callback(hObject, eventdata, handles)
% hObject    handle to q18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q18 as text
%        str2double(get(hObject,'String')) returns contents of q18 as a double


% --- Executes during object creation, after setting all properties.
function q18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit38_Callback(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit38 as text
%        str2double(get(hObject,'String')) returns contents of edit38 as a double


% --- Executes during object creation, after setting all properties.
function edit38_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit39_Callback(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit39 as text
%        str2double(get(hObject,'String')) returns contents of edit39 as a double


% --- Executes during object creation, after setting all properties.
function edit39_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit40_Callback(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit40 as text
%        str2double(get(hObject,'String')) returns contents of edit40 as a double


% --- Executes during object creation, after setting all properties.
function edit40_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q2_Callback(hObject, eventdata, handles)
% hObject    handle to q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q2 as text
%        str2double(get(hObject,'String')) returns contents of q2 as a double


% --- Executes during object creation, after setting all properties.
function q2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q1_Callback(hObject, eventdata, handles)
% hObject    handle to q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of q1 as text
%        str2double(get(hObject,'String')) returns contents of q1 as a double


% --- Executes during object creation, after setting all properties.
function q1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes6


% --- Executes during object creation, after setting all properties.
function axes7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes7


% --- Executes during object creation, after setting all properties.
function axes9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes9


% --- Executes during object creation, after setting all properties.
function axes10_CreateFcn(hObject, eventdata, handles)
axis off;
% hObject    handle to axes10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes10


% --- Executes during object creation, after setting all properties.
function axes11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes11


% --- Executes during object creation, after setting all properties.
function axes12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes12


% --- Executes during object creation, after setting all properties.
function axes17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes17


% --- Executes during object creation, after setting all properties.
function axes13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes13


% --- Executes during object creation, after setting all properties.
function axes14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes14


% --- Executes during object creation, after setting all properties.
function axes15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes15


% --- Executes during object creation, after setting all properties.
function axes16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes16


% --- Executes during object creation, after setting all properties.
function axes8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes8


% --- Executes during object creation, after setting all properties.
function axes18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes18


% --- Executes during object creation, after setting all properties.
function axes19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes19


% --- Executes during object creation, after setting all properties.
function axes20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes20


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global j;
global ListOfImageNames;
global folder_name;
global fileNames;
global Selected;
if(j>1)
j=j-1;
info = dicominfo(fullfile(folder_name,fileNames{j}));
 handles.info_dicom=info;
     guidata(hObject,handles);
if isfield(info,'PatientName')set(handles.q1,'string',info.PatientName.FamilyName);else  set(handles.q1,'string','');end;
if isfield(info,'PatientID')set(handles.q2,'string',info.PatientID); else  set(handles.q2,'string','');end;
if isfield(info,'PatientSex')set(handles.q3,'string',info.PatientSex);else  set(handles.q3,'string','');end;
if isfield(info,'PatientBirthDate')set(handles.q3,'string',info.PatientBirthDate);else  set(handles.q3,'string','');end;
if isfield(info,'PatientWeight')set(handles.q5,'string',info.PatientWeight);else  set(handles.q5,'string','');end;    
if isfield(info,'PatientAge')set(handles.q5,'string',info.PatientAge);else  set(handles.q5,'string',''); end; 
if isfield(info,'PatientAddress')set(handles.q7,'string',info.PatientAddress);else  set(handles.q7,'string','');end;
       
if isfield(info,'StudyDate')set(handles.q8,'string',info.StudyDate);else  set(handles.q8,'string',''); end;
if isfield(info,'StudyTime')set(handles.q9,'string',info.StudyTime);else  set(handles.q9,'string','');end;
if isfield(info,'StudyID')set(handles.q10,'string',info.StudyID);else  set(handles.q10,'string','');end;
if isfield(info,'Modality')set(handles.q11,'string',info.Modality);else  set(handles.q11,'string',''); end;
if isfield(info,'StudyDescription')set(handles.q12,'string',info.StudyDescription);else  set(handles.q12,'string','');end;

if isfield(info,'SeriesDate')set(handles.q13,'string',info.SeriesDate);else  set(handles.q13,'string','');end;
if isfield(info,'SeriesTime')set(handles.q14,'string',info.SeriesTime);else  set(handles.q14,'string','');end;
if isfield(info,'SeriesDescription')set(handles.q15,'string',info.SeriesDescription);else  set(handles.q15,'string','');end;
if isfield(info,'ProtocolName')set(handles.q16,'string',info.ProtocolName);else  set(handles.q16,'string','');end;
if isfield(info,'PatientPosition')set(handles.q17,'string',info.PatientPosition);else  set(handles.q17,'string','');end;
if isfield(info,'SeriesNumber')set(handles.q18,'string',info.SeriesNumber);else  set(handles.q18,'string','');end;


%extract size info from metadata

%Read one file to get size
I       = dicomread(fullfile(folder_name,fileNames{1}));
classI  = class(I);
sizeI   = size(I);
numImages= length(fileNames);
name=info.PatientName;
name=struct2cell(name);
%DISPLAY IMAGES


if(j>0 & j< numImages)
axes(handles.axes6);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;

if(j>4 & j< numImages)
axes(handles.axes9);
demo=dicomread(fullfile(folder_name,fileNames{j-4}));
imshow(demo,[]);else
    axes(handles.axes9);
    imshow(0.94,[]);
end;

if(j>3 & j< numImages)
axes(handles.axes10);
demo=dicomread(fullfile(folder_name,fileNames{j-3}));
imshow(demo,[]);else
    axes(handles.axes10);
    imshow(0.94,[]);
end;

if(j>2 & j< numImages)
axes(handles.axes11);
demo=dicomread(fullfile(folder_name,fileNames{j-2}));
imshow(demo,[]);else
    axes(handles.axes11);
    imshow(0.94,[]);
end;

if(j>1 & j< numImages)
axes(handles.axes12);
demo=dicomread(fullfile(folder_name,fileNames{j-1}));
imshow(demo,[]);else
    axes(handles.axes12);
    imshow(0.94,[]);
end;

if(j>-1 & j< numImages-1)
axes(handles.axes13);
demo=dicomread(fullfile(folder_name,fileNames{j+1}));
imshow(demo,[]);else
    axes(handles.axes13);
    imshow(0.94,[]);
end;

if(j>-2 & j< numImages-2)
axes(handles.axes14);
demo=dicomread(fullfile(folder_name,fileNames{j+2}));
imshow(demo,[]);else
    axes(handles.axes14);
    imshow(0.94,[]);
end;

if(j>-3 & j< numImages-3)
axes(handles.axes15);
demo=dicomread(fullfile(folder_name,fileNames{j+3}));
imshow(demo,[]);else
    axes(handles.axes15);
    imshow(0.94,[]);
end;

if(j>-4 & j< numImages-4)
axes(handles.axes16);
demo=dicomread(fullfile(folder_name,fileNames{j+4}));
imshow(demo,[]);else
    axes(handles.axes16);
    imshow(0.94,[]);
end;

if(j>0 & j< numImages)
axes(handles.axes17);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;
y=sprintf('Slice %d/%d ',Selected-1,length(ListOfImageNames));
set(handles.num,'string',y);
set(handles.listbox2, 'value', get(handles.listbox2, 'value')-1);
Selected=Selected-1;
end;


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global j;
global ListOfImageNames;
global folder_name;
global fileNames;
global Selected;
if(j<length(fileNames))
j=j+1;
info = dicominfo(fullfile(folder_name,fileNames{j}));
 handles.info_dicom=info;
     guidata(hObject,handles);
if isfield(info,'PatientName')set(handles.q1,'string',info.PatientName.FamilyName);else  set(handles.q1,'string','');end;
if isfield(info,'PatientID')set(handles.q2,'string',info.PatientID); else  set(handles.q2,'string','');end;
if isfield(info,'PatientSex')set(handles.q3,'string',info.PatientSex);else  set(handles.q3,'string','');end;
if isfield(info,'PatientBirthDate')set(handles.q3,'string',info.PatientBirthDate);else  set(handles.q3,'string','');end;
if isfield(info,'PatientWeight')set(handles.q5,'string',info.PatientWeight);else  set(handles.q5,'string','');end;    
if isfield(info,'PatientAge')set(handles.q5,'string',info.PatientAge);else  set(handles.q5,'string',''); end; 
if isfield(info,'PatientAddress')set(handles.q7,'string',info.PatientAddress);else  set(handles.q7,'string','');end;
       
if isfield(info,'StudyDate')set(handles.q8,'string',info.StudyDate);else  set(handles.q8,'string',''); end;
if isfield(info,'StudyTime')set(handles.q9,'string',info.StudyTime);else  set(handles.q9,'string','');end;
if isfield(info,'StudyID')set(handles.q10,'string',info.StudyID);else  set(handles.q10,'string','');end;
if isfield(info,'Modality')set(handles.q11,'string',info.Modality);else  set(handles.q11,'string',''); end;
if isfield(info,'StudyDescription')set(handles.q12,'string',info.StudyDescription);else  set(handles.q12,'string','');end;

if isfield(info,'SeriesDate')set(handles.q13,'string',info.SeriesDate);else  set(handles.q13,'string','');end;
if isfield(info,'SeriesTime')set(handles.q14,'string',info.SeriesTime);else  set(handles.q14,'string','');end;
if isfield(info,'SeriesDescription')set(handles.q15,'string',info.SeriesDescription);else  set(handles.q15,'string','');end;
if isfield(info,'ProtocolName')set(handles.q16,'string',info.ProtocolName);else  set(handles.q16,'string','');end;
if isfield(info,'PatientPosition')set(handles.q17,'string',info.PatientPosition);else  set(handles.q17,'string','');end;
if isfield(info,'SeriesNumber')set(handles.q18,'string',info.SeriesNumber);else  set(handles.q18,'string','');end;


%extract size info from metadata

%Read one file to get size
I       = dicomread(fullfile(folder_name,fileNames{1}));
classI  = class(I);
sizeI   = size(I);
numImages= length(fileNames);
name=info.PatientName;
name=struct2cell(name);
%DISPLAY IMAGES


if(j>0 & j< numImages)
axes(handles.axes6);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;

if(j>4 & j< numImages)
axes(handles.axes9);
demo=dicomread(fullfile(folder_name,fileNames{j-4}));
imshow(demo,[]);else
    axes(handles.axes9);
    imshow(0.94,[]);
end;

if(j>3 & j< numImages)
axes(handles.axes10);
demo=dicomread(fullfile(folder_name,fileNames{j-3}));
imshow(demo,[]);else
    axes(handles.axes10);
    imshow(0.94,[]);
end;

if(j>2 & j< numImages)
axes(handles.axes11);
demo=dicomread(fullfile(folder_name,fileNames{j-2}));
imshow(demo,[]);else
    axes(handles.axes11);
    imshow(0.94,[]);
end;

if(j>1 & j< numImages)
axes(handles.axes12);
demo=dicomread(fullfile(folder_name,fileNames{j-1}));
imshow(demo,[]);else
    axes(handles.axes12);
    imshow(0.94,[]);
end;

if(j>-1 & j< numImages-1)
axes(handles.axes13);
demo=dicomread(fullfile(folder_name,fileNames{j+1}));
imshow(demo,[]);else
    axes(handles.axes13);
    imshow(0.94,[]);
end;

if(j>-2 & j< numImages-2)
axes(handles.axes14);
demo=dicomread(fullfile(folder_name,fileNames{j+2}));
imshow(demo,[]);else
    axes(handles.axes14);
    imshow(0.94,[]);
end;

if(j>-3 & j< numImages-3)
axes(handles.axes15);
demo=dicomread(fullfile(folder_name,fileNames{j+3}));
imshow(demo,[]);else
    axes(handles.axes15);
    imshow(0.94,[]);
end;

if(j>-4 & j< numImages-4)
axes(handles.axes16);
demo=dicomread(fullfile(folder_name,fileNames{j+4}));
imshow(demo,[]);else
    axes(handles.axes16);
    imshow(0.94,[]);
end;

if(j>0 & j< numImages)
axes(handles.axes17);
demo=dicomread(fullfile(folder_name,fileNames{j}));
imshow(demo,[]);
end;
set(handles.listbox2, 'value', get(handles.listbox2, 'value')+1);
y=sprintf('Slice %d/%d ',Selected+1,length(ListOfImageNames));
set(handles.num,'string',y);
Selected=Selected+1;
end;
% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mri slider2 voxel_size h
im=mri(:,:,10);
max_level=double(max(im(:)));
%% Isolate and Display brain 
%Apply some thresholding rules to ignore certain parts of data
mriTemp=mri(:,:,10);
mriTemp(max_level:end,:)=0;
%% threshold on relaxation times
%Use morphological operations and blob analysis to exclude non-brain tissue
%Convert to black and white and dilate work on all the images
%Convert image to B/N and remove small blobs
lb=40;
ub=100;
mriAdjust=mri;
mriAdjust(mriAdjust<=lb)=0;
mriAdjust(mriAdjust>=ub)=0;
mriAdjust(slider2:end,:,:)=0;
bw=logical(mriAdjust);
%% let's clea that up a bit
nhood=ones([7 7 3]);
%morphologically open image
bw=imopen(bw,nhood);
%% Find blobs
L=bwlabeln(bw);
stats=regionprops(L,'Area','Centroid');
LL=L(:,:,10)+1;
cmap=hsv(length(stats));cmap=[0 0 0;cmap];
%determine the largest blob and eliminate all other blobs
A=[stats.Area];
biggest=find(A == max(A));
mriAdjust(L~=biggest)=0;
%% Partition brain mass
%separate the brain mass into 2categories
level = thresh_tool(uint16(mriAdjust(:,:,10)),'gray');
mriBrainPartition=uint8(zeros(size(mriAdjust)));
mriBrainPartition(mriAdjust<level & mriAdjust>0)=2;
mriBrainPartition(mriAdjust>=level) =3;
%% Creat 3-D display
tmp=text(128,200,'Rendering 3-D: Please wait!','color','r','fontsize',14,...
  'horizontalalignment','center');
Ds=imresize(mriBrainPartition,0.25,'nearest');
Ds=flipud(Ds);
Ds=fliplr(Ds);
Ds=permute(Ds,[3 2 1]);
voxel_size2=voxel_size([1 3 2]).*[4 1 4];
white_vol=isosurface(Ds,2.5);
gray_vol=isosurface(Ds,1.5);

h=figure('visible','off','outerposition',[0 0 800 600],'renderer','openGL','WindowStyle','normal');
patch(white_vol,'FaceColor','b','EdgeColor','none');
patch(gray_vol,'FaceColor','y','EdgeColor','none','FaceAlpha',0.5);
view(45,15);daspect(1./voxel_size2);axis tight;axis off;
camlight;camlight(-80,-10);lighting phong;
delete(tmp);
movegui(h,'center');
set(h,'visible','on');

% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global folder_name;
global fileNames;
global j;
for i = 1:length(fileNames)
  %extract size info from metadata
  set(handles.listbox2, 'value', i);
 info = dicominfo(fullfile(folder_name,fileNames{i}));
 handles.info_dicom=info;
     guidata(hObject,handles);
axes(handles.axes6);
demo=dicomread(fullfile(folder_name,fileNames{i}));
imshow(demo,[]);
%Read one file to get size
pause(get(handles.slider7,'Value'));


%DISPLAY IMAGES
end;


% --- Executes on slider movement.
function slider7_Callback(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

speed=get(handles.slider7,'Value');
set(handles.speed,'string',num2str(speed));
% --- Executes during object creation, after setting all properties.
function slider7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --------------------------------------------------------------------
function zoom_Callback(hObject, eventdata, handles)
% hObject    handle to zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom on;



function data_Callback(hObject, eventdata, handles)
% hObject    handle to data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of data as text
%        str2double(get(hObject,'String')) returns contents of data as a double


% --- Executes during object creation, after setting all properties.
function data_CreateFcn(hObject, eventdata, handles)
% hObject    handle to data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function speed_Callback(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of speed as text
%        str2double(get(hObject,'String')) returns contents of speed as a double


% --- Executes during object creation, after setting all properties.
function speed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function StopVideo_Callback(hObject, eventdata, handles)
% hObject    handle to StopVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function contact_Callback(hObject, eventdata, handles)
% hObject    handle to contact (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiwait(msgbox({'He thong PACS';'GVHD: Ths Hoang Quang Huy';'             TS.Nguyen Thu Van';'Nhom thuc hien:';
    'Tran Duc Chinh         chinh.td150387@sis.hust.edu.vn';
    ' Nguyen Duc Trung   trung.nd152828@sis.hust.edu.vn'}));
