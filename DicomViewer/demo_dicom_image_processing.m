function varargout = demo_dicom_image_processing(varargin)
% DEMO_DICOM_IMAGE_PROCESSING MATLAB code for demo_dicom_image_processing.fig
%      DEMO_DICOM_IMAGE_PROCESSING, by itself, creates a new DEMO_DICOM_IMAGE_PROCESSING or raises the existing
%      singleton*.
%
%      H = DEMO_DICOM_IMAGE_PROCESSING returns the handle to a new DEMO_DICOM_IMAGE_PROCESSING or the handle to
%      the existing singleton*.
%
%      DEMO_DICOM_IMAGE_PROCESSING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEMO_DICOM_IMAGE_PROCESSING.M with the given input arguments.
%
%      DEMO_DICOM_IMAGE_PROCESSING('Property','Value',...) creates a new DEMO_DICOM_IMAGE_PROCESSING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before demo_dicom_image_processing_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to demo_dicom_image_processing_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help demo_dicom_image_processing

% Last Modified by GUIDE v2.5 06-May-2019 23:45:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @demo_dicom_image_processing_OpeningFcn, ...
                   'gui_OutputFcn',  @demo_dicom_image_processing_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before demo_dicom_image_processing is made visible.
function demo_dicom_image_processing_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to demo_dicom_image_processing (see VARARGIN)

% Choose default command line output for demo_dicom_image_processing
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes demo_dicom_image_processing wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = demo_dicom_image_processing_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function file_Callback(hObject, eventdata, handles)
% hObject    handle to file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function view_Callback(hObject, eventdata, handles)
% hObject    handle to view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function tools_Callback(hObject, eventdata, handles)
% hObject    handle to tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function about_Callback(hObject, eventdata, handles)
% hObject    handle to about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function convert_Callback(hObject, eventdata, handles)
% hObject    handle to convert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function zoom_Callback(hObject, eventdata, handles)
% hObject    handle to zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
zoom on;
% --------------------------------------------------------------------
function ctrast_Callback(hObject, eventdata, handles)
% hObject    handle to ctrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
imtool(I,'DisplayRange',[]);


% --------------------------------------------------------------------
function rot_Callback(hObject, eventdata, handles)
% hObject    handle to rot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function open_Callback(hObject, eventdata, handles)
% hObject    handle to open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.openi,'Enable','off');
[filename, pathname] = uigetfile('.dcm', 'Open DICOM Image');% Open only DICOM image
if isequal(filename, 0) || isequal(pathname, 0)   
    msgbox('Image input canceled.');  
else
 I=dicomread(filename);
end
handles.axes1;
imshow(I,'DisplayRange',[]);
infor=dicominfo(filename);
pn=infor.PatientName;
pn=struct2cell(pn);
set(handles.pid,'string',infor.PatientID);
set(handles.pname,'string',pn);
set(handles.modal,'string',infor.Modality);
set(handles.dob,'string',infor.PatientBirthDate);
set(handles.psex,'string',infor.PatientSex);
set(handles.style,'string',infor.ColorType);
set(handles.sdate,'string',infor.StudyDate);
set(handles.stime,'string',infor.StudyTime);
set(handles.des,'string',infor.StudyID);
set(handles.des,'string',infor.StudyDescription);

% --------------------------------------------------------------------
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function exit_Callback(hObject, eventdata, handles)
% hObject    handle to exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make Y/N question to exit
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end

% --------------------------------------------------------------------
function jpg_Callback(hObject, eventdata, handles)
% hObject    handle to jpg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function gif_Callback(hObject, eventdata, handles)
% hObject    handle to gif (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function png_Callback(hObject, eventdata, handles)
% hObject    handle to png (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function tiff_Callback(hObject, eventdata, handles)
% hObject    handle to tiff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function help_Callback(hObject, eventdata, handles)
% hObject    handle to help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function hw2_Callback(hObject, eventdata, handles)
% hObject    handle to hw2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
web Homework2.htm
% --------------------------------------------------------------------
function his_Callback(hObject, eventdata, handles)
% hObject    handle to his (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% y=textread('HIS.txt', '%s', 'whitespace', '');
% msgbox(y,'Health Information System');
url='https://digitalguardian.com/blog/what-health-information-system';
web(url);
% --------------------------------------------------------------------
function app_Callback(hObject, eventdata, handles)
% hObject    handle to app (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
web Application.htm;
% --------------------------------------------------------------------
function source_Callback(hObject, eventdata, handles)
% hObject    handle to source (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y=textread('source.txt', '%s', 'whitespace', '');
msgbox(y,'Contact to view code');

% --- Executes on button press in transformi.
function transformi_Callback(hObject, eventdata, handles)
% hObject    handle to transformi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
c=get(handles.typei,'value');
switch c
    case 1
    answer = inputdlg('Enter value thresold of image','Thresold of value',[1 35]);
    answer=str2double(answer);
    bw_image=im2bw(I,answer );
    imshow(bw_image);
    handles.bw=bw_image;
    case 2
     y=grs2rgb(I,hsv);
     imshow(y);
     handles.rgb=y;
end
guidata(hObject,handles);
% --- Executes on button press in converti.
function converti_Callback(hObject, eventdata, handles)
% hObject    handle to converti (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
myicon=imread('ok_1.jpg');
c=get(handles.formati,'value');
[filename path]=uiputfile();
if(c==1)
imwrite(I,sprintf('%s.jpg',filename), 'jpg');% Save Image as Jpeg format
msgbox('Finished saving .jpg image','Complete','custom',myicon);
elseif(c==2)
imwrite(I,sprintf('%s.png',filename), 'png');% Save Image as Png format
msgbox('Finished saving .png image','Complete','custom',myicon);
elseif(c==3)
imwrite(I,sprintf('%s.tiff',filename), 'tiff');% Save Image as Tiff format
msgbox('Finished saving .tiff image','Complete','custom',myicon);
elseif(c==4)
imwrite(I,sprintf('%s.gif',filename), 'gif');% Save Image as Gif format
msgbox('Finished saving .gif image','Complete','custom',myicon);
else
    msgbox('Unknown image format name.');
end; 

% --- Executes on selection change in typei.
function typei_Callback(hObject, eventdata, handles)
% hObject    handle to typei (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns typei contents as cell array
%        contents{get(hObject,'Value')} returns selected item from typei


% --- Executes during object creation, after setting all properties.
function typei_CreateFcn(hObject, eventdata, handles)
% hObject    handle to typei (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in formati.
function formati_Callback(hObject, eventdata, handles)
% hObject    handle to formati (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns formati contents as cell array
%        contents{get(hObject,'Value')} returns selected item from formati


% --- Executes during object creation, after setting all properties.
function formati_CreateFcn(hObject, eventdata, handles)
% hObject    handle to formati (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in openi.
function openi_Callback(hObject, eventdata, handles)
% hObject    handle to openi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global I
[filename, pathname] = uigetfile('.dcm', 'Open DICOM Image');% Open only DICOM image
if isequal(filename, 0) || isequal(pathname, 0)   
    msgbox('Image input canceled.');  
else
 I=dicomread(filename);
end
I=uint8(255 * mat2gray(I))
handles.axes1;
imshow(I,'DisplayRange',[]);
infor=dicominfo(filename);
pn=infor.PatientName;
pn=struct2cell(pn);
set(handles.pid,'string',infor.PatientID);
set(handles.pname,'string',pn);
set(handles.modal,'string',infor.Modality);
set(handles.dob,'string',infor.PatientBirthDate);
set(handles.psex,'string',infor.PatientSex);
set(handles.style,'string',infor.ColorType);
set(handles.sdate,'string',infor.StudyDate);
set(handles.stime,'string',infor.StudyTime);
set(handles.des,'string',infor.StudyID);
set(handles.des,'string',infor.StudyDescription);


% --- Executes on button press in rotatei.
function rotatei_Callback(hObject, eventdata, handles)
% hObject    handle to rotatei (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
 m = inputdlg('Enter value degree of rotation','Degree of rotation',[1 35]);
 m=str2double(m);
 y=imrotate(I,m,'bilinear');
handles.axes1;
imshow(y);
handles.y=y;
guidata(hObject, handles);

function deg_Callback(hObject, eventdata, handles)
% hObject    handle to deg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of deg as text
%        str2double(get(hObject,'String')) returns contents of deg as a double


% --- Executes during object creation, after setting all properties.
function deg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to deg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function r90_Callback(hObject, eventdata, handles)
% hObject    handle to r90 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
y=imrotate(I,90,'bilinear');
handles.axes1;
imshow(y);

% --------------------------------------------------------------------
function r180_Callback(hObject, eventdata, handles)
% hObject    handle to r180 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global I
y=imrotate(I,180,'bilinear');
handles.axes1;
imshow(y);

% --- Executes on button press in savei.
function savei_Callback(hObject, eventdata, handles)
% hObject    handle to savei (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y=handles.y;
myicon=imread('ok_1.jpg');
[filename pathname]=uiputfile('*.*');
dicomwrite(y,sprintf('%s.dcm',filename));
msgbox('Complete saved new image by format.dcm','Complete','custom',myicon);
% --- Executes on button press in exitp.
function exitp_Callback(hObject, eventdata, handles)
% hObject    handle to exitp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end


% --- Executes on button press in sv1.
function sv1_Callback(hObject, eventdata, handles)
% hObject    handle to sv1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
c=get(handles.typei,'value');
[filename path]=uiputfile('*.*');
myicon=imread('ok_1.jpg');
switch c
    case 1
        dicomwrite(handles.bw,sprintf('%s.dcm',filename));
        msgbox('The new binary image saved by format .dcm','Complete','custom',myicon);
    case 2
        dicomwrite(handles.rgb,sprintf('%s.dcm',filename));
        msgbox('The new rgb image saved  by format .dcm','Complete','custom',myicon);
    otherwise
        msgbox('Please choose type of image','Error','warn');
end


% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(findobj('style','pushbutton'), 'Value', 0,'Enable','on');
cla(handles.axes1,'reset');
set(handles.axes1,'visible','off');
set(handles.pid,'string','');
set(handles.pname,'string','');
set(handles.modal,'string','');
set(handles.dob,'string','');
set(handles.psex,'string','');
set(handles.style,'string','');
set(handles.sdate,'string','');
set(handles.stime,'string','');
set(handles.des,'string','');
set(handles.des,'string','');
