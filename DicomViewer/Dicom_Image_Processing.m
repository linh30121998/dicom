function varargout = Dicom_Image_Processing(varargin)
% DICOM_IMAGE_PROCESSING MATLAB code for Dicom_Image_Processing.fig
%      DICOM_IMAGE_PROCESSING, by itself, creates a new DICOM_IMAGE_PROCESSING or raises the existing
%      singleton*.
%
%      H = DICOM_IMAGE_PROCESSING returns the handle to a new DICOM_IMAGE_PROCESSING or the handle to
%      the existing singleton*.
%
%      DICOM_IMAGE_PROCESSING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DICOM_IMAGE_PROCESSING.M with the given input arguments.
%
%      DICOM_IMAGE_PROCESSING('Property','Value',...) creates a new DICOM_IMAGE_PROCESSING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Dicom_Image_Processing_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Dicom_Image_Processing_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Dicom_Image_Processing

% Last Modified by GUIDE v2.5 18-May-2019 13:43:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Dicom_Image_Processing_OpeningFcn, ...
                   'gui_OutputFcn',  @Dicom_Image_Processing_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Dicom_Image_Processing is made visible.
function Dicom_Image_Processing_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Dicom_Image_Processing (see VARARGIN)

% Choose default command line output for Dicom_Image_Processing
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Dicom_Image_Processing wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Dicom_Image_Processing_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
y=get(handles.checkbox1,'Value');
if y==1
set(handles.checkbox2,'Enable','off');
else 
 set(handles.checkbox2,'Enable','on');
end

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
y=get(handles.checkbox2,'Value');
if y==1
set(handles.checkbox1,'Enable','off');
else 
 set(handles.checkbox1,'Enable','on');
end


% --- Executes on button press in next.
function next_Callback(hObject, eventdata, handles)
% hObject    handle to next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
y=get(handles.checkbox1,'Value');
if y==1
%     set(handles.checkbox2,'Enable','off');
    run demo_dicom_image_processing.m
else
    run creat_3d.m
end


% --- Executes on button press in EXIT.
function EXIT_Callback(hObject, eventdata, handles)
% hObject    handle to EXIT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Do you want to exit?', ...
	'Exit Program', ...
	'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
       close;
    case 'No'
      pause(0.1);
end
