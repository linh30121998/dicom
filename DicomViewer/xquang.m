function varargout = xquang(varargin)
% XQUANG MATLAB code for xquang.fig
%      XQUANG, by itself, creates a new XQUANG or raises the existing
%      singleton*.
%
%      H = XQUANG returns the handle to a new XQUANG or the handle to
%      the existing singleton*.
%
%      XQUANG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in XQUANG.M with the given input arguments.
%
%      XQUANG('Property','Value',...) creates a new XQUANG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before xquang_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to xquang_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help xquang

% Last Modified by GUIDE v2.5 01-Jul-2020 02:05:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @xquang_OpeningFcn, ...
                   'gui_OutputFcn',  @xquang_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before xquang is made visible.
function xquang_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to xquang (see VARARGIN)
global insert;insert=3;
logo=imread('logobyt1.png');
axes(handles.axes1);
imshow(logo,[]);
d = datetime('today');
t= datestr(d);
ngaythang=strsplit(t,'-');
 set(handles.ngay,'string',ngaythang(1));
 switch char(ngaythang(2))
     case char('Jan')
         thang=1;
     case char('Feb')
         thang =2;
     case char('Mar')
         thang=3;
     case char('Apr')
         thang =4;
     case char('May')
         thang=5;
     case char('Jun')
         thang=6;
     case char('Jul')
         thang=7;
     case char('Aug')
         thang=8;
     casechar('Sep')
         thang=9;
     case char('Oct')
         thang=10;
     case char('Nov')
         thang=11;
     otherwise
         thang=12;
 end;
 set(handles.thang,'string',thang);
 set(handles.nam,'string',ngaythang(3));
global file;
global filename;
global open;global j;
global ListOfImageNames;
global folder_name;
global fileNames;
global Selected;
if(open==1)data1=dicominfo(file);anh=file;
else anh=fullfile(folder_name,fileNames{j});
    data1=dicominfo(fullfile(folder_name,fileNames{j}));
end;
 img =dicomread(anh);
axes(handles.axes2);
imshow(img,[]);tenphieu='MRI';
if isfield(data1,'PatientName') set(handles.hoten,'string',data1.PatientName.FamilyName);else  set(handles.hoten,'string','');end;
if isfield(data1,'PatientAddress') set(handles.diachi,'string',data1.PatientAddress);else  set(handles.diachi,'string','');end;
if isfield(data1,'PatientBirthDate') set(handles.namsinh,'string',data1.PatientBirthDate);
 if(data1.PatientBirthDate~=0)  s=get(handles.namsinh,'string');
ng = strcat(s(7),s(8));th=strcat(s(5),s(6));n=strcat(s(1),s(2),s(3),s(4));
set(handles.ngaysinh,'string',ng);
set(handles.thangsinh,'string',th);
set(handles.namsinh,'string',n);else  set(handles.thangsinh,'string','');
 set(handles.ngaysinh,'string','');
 set(handles.namsinh,'string','');end;
else  set(handles.thangsinh,'string','');
 set(handles.ngaysinh,'string','');
 set(handles.namsinh,'string','');end;
tuoi=num2str(2020-str2num(n));
set(handles.tuoi,'string',tuoi);


switch char(data1.Modality)
    case char('MR')
        tenphieu='MRI';
    case char('CT')
        tenphieu='CT';
    case char('US')
        tenphieu='SIEU AM';
    otherwise
        tenphieu='X-Quang';
end;
    set(handles.modality,'string',tenphieu);
if(char(data1.PatientSex)=='M')
     set(handles.gioitinh,'string','Nam');end;
 %kyten
 global chuky;
 if(chuky==1)
     chuky_hinh=imread('chinh_chuky.jpg');
     axes(handles.axes4);
     imshow(chuky_hinh,[]);
     set(handles.kyten,'Visible','on');
     set(handles.chandoan,'enable','off');
     set(handles.yeucau,'enable','off');
     set(handles.ketqua,'enable','off');
 else set(handles.kyten,'Visible','off');
 end;
 
  if(chuky==2)
     chuky_hinh2=imread('chuky_trung.jpg');
     axes(handles.axes4);
     imshow(chuky_hinh2,[]);
     set(handles.trungky,'Visible','on');
     set(handles.chandoan,'enable','off');
     set(handles.yeucau,'enable','off');
     set(handles.ketqua,'enable','off');
 else set(handles.trungky,'Visible','off');
 end;
 
 global huyki;
 if(huyki==1)
     axes(handles.axes4);
imshow(1,[]);
set(handles.chandoan,'enable','on');
     set(handles.yeucau,'enable','on');
     set(handles.ketqua,'enable','on');
     set(handles.trungky,'Visible','off');
set(handles.kyten,'Visible','off');end;
 if (huyki==2)
 axes(handles.axes4);
imshow(1,[]);
set(handles.chandoan,'enable','on');
     set(handles.yeucau,'enable','on');
     set(handles.ketqua,'enable','on');
     set(handles.trungky,'Visible','off');
set(handles.kyten,'Visible','off');
 end;
% Choose default command line output for xquang
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes xquang wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = xquang_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function chandoan_Callback(hObject, eventdata, handles)
% hObject    handle to chandoan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of chandoan as text
%        str2double(get(hObject,'String')) returns contents of chandoan as a double


% --- Executes during object creation, after setting all properties.
function chandoan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chandoan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yeucau_Callback(hObject, eventdata, handles)
% hObject    handle to yeucau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yeucau as text
%        str2double(get(hObject,'String')) returns contents of yeucau as a double


% --- Executes during object creation, after setting all properties.
function yeucau_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yeucau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ketqua_Callback(hObject, eventdata, handles)
% hObject    handle to ketqua (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ketqua as text
%        str2double(get(hObject,'String')) returns contents of ketqua as a double


% --- Executes during object creation, after setting all properties.
function ketqua_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ketqua (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
axis off;
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes1


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function inputSign_Callback(hObject, eventdata, handles)
% hObject    handle to inputSign (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
kyten;
% --------------------------------------------------------------------
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  temp_fname = [tempname '.png'];%Create a temporary name 
print(['-f' sprintf('%d',figure_number)], '-dpng', temp_fname);
Word_COM = actxserver('Word.Application');
 set(Word_COM,'Visible',1);
 File_COM = Word_COM.Documents.Add;
 invoke(File_COM)
Word_COM.Selection.InlineShapes.AddPicture(temp_fname);
delete(temp_fname);
set(Pic, 'ScaleHeight', scale, 'ScaleWidth', scale);


function hoten_Callback(hObject, eventdata, handles)
% hObject    handle to hoten (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of hoten as text
%        str2double(get(hObject,'String')) returns contents of hoten as a double


% --- Executes during object creation, after setting all properties.
function hoten_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hoten (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function diachi_Callback(hObject, eventdata, handles)
% hObject    handle to diachi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of diachi as text
%        str2double(get(hObject,'String')) returns contents of diachi as a double


% --- Executes during object creation, after setting all properties.
function diachi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to diachi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function khoa_Callback(hObject, eventdata, handles)
% hObject    handle to khoa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of khoa as text
%        str2double(get(hObject,'String')) returns contents of khoa as a double


% --- Executes during object creation, after setting all properties.
function khoa_CreateFcn(hObject, eventdata, handles)
% hObject    handle to khoa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tuoi_Callback(hObject, eventdata, handles)
% hObject    handle to tuoi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tuoi as text
%        str2double(get(hObject,'String')) returns contents of tuoi as a double


% --- Executes during object creation, after setting all properties.
function tuoi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tuoi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sobhyt_Callback(hObject, eventdata, handles)
% hObject    handle to sobhyt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sobhyt as text
%        str2double(get(hObject,'String')) returns contents of sobhyt as a double


% --- Executes during object creation, after setting all properties.
function sobhyt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sobhyt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function insert_Callback(hObject, eventdata, handles)
% hObject    handle to insert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global insert;
if(insert==3)
[file2 path2] = uigetfile({'*.dcm'},'File Selector');
handles.filedcm1 = strcat(path2, file2);
file3=handles.filedcm1;
guidata(hObject, handles);
folder_name1=[file2,path2];
   axes(handles.axes3);img3 = dicomread(handles.filedcm1);
 imshow(img3,[]);
 insert=insert+2;
elseif(insert==5)
    [file3 path3] = uigetfile({'*.dcm'},'File Selector');
handles.filedcm2 = strcat(path3, file3);
file3=handles.filedcm2;
guidata(hObject, handles);
folder_name1=[file3,path3];
   axes(handles.axes5);img4 = dicomread(handles.filedcm2);
 imshow(img4,[]);
 insert=insert+2;
end;
 
% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes3


% --- Executes during object creation, after setting all properties.
function axes4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
axis off;
% Hint: place code in OpeningFcn to populate axes4


% --------------------------------------------------------------------
function print_Callback(hObject, eventdata, handles)
% hObject    handle to print (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    print(gcf)
    % Find end of document and make it the insertion point:
    end_of_doc = get(actx_word_p.activedocument.content,'end');
    set(actx_word_p.application.selection,'Start',end_of_doc);
    set(actx_word_p.application.selection,'End',end_of_doc);
    % Paste the contents of the Clipboard:
        %also works Paste(ActXWord.Selection)
    invoke(actx_word_p.Selection,'Paste');


% --------------------------------------------------------------------
function close_Callback(hObject, eventdata, handles)
% hObject    handle to close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global chuky;
chuky=0;
close;


% --------------------------------------------------------------------
function huyky_Callback(hObject, eventdata, handles)
% hObject    handle to huyky (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global huyki;
huyky;
