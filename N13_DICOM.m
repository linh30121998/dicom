function varargout = N13_DICOM(varargin)
% N13_DICOM MATLAB code for N13_DICOM.fig
%      N13_DICOM, by itself, creates a new N13_DICOM or raises the existing
%      singleton*.
%
%      H = N13_DICOM returns the handle to a new N13_DICOM or the handle to
%      the existing singleton*.
%
%      N13_DICOM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in N13_DICOM.M with the given input arguments.
%
%      N13_DICOM('Property','Value',...) creates a new N13_DICOM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before N13_DICOM_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to N13_DICOM_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help N13_DICOM

% Last Modified by GUIDE v2.5 17-Nov-2020 20:12:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @N13_DICOM_OpeningFcn, ...
                   'gui_OutputFcn',  @N13_DICOM_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before N13_DICOM is made visible.
function N13_DICOM_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to N13_DICOM (see VARARGIN)

% Choose default command line output for N13_DICOM
handles.output = hObject;
handles.n = 1;
handles.dem =1.5;
handles.soanh = 1;
handles.filenames = [1000,1000];
handles.x = 2500;
handles.image_folder = '';
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes N13_DICOM wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = N13_DICOM_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Image_Callback(hObject, eventdata, handles)
% hObject    handle to Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Measure_and_Annotate_Callback(hObject, eventdata, handles)
% hObject    handle to Measure_and_Annotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Help_Callback(hObject, eventdata, handles)
% hObject    handle to Help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function About_N13_DICOM_Callback(hObject, eventdata, handles)
% hObject    handle to About_N13_DICOM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
msgbox({'GVHD: Ths Hoang Quang Huy','          ---Thong tin thanh vien nhom 13---','1. Nguyen Thi Thuy Linh - 20167260','2.Truong Tuan Vu - 20164724'});

% --------------------------------------------------------------------
function Distance_Callback(hObject, eventdata, handles)
% hObject    handle to Distance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1)
h = imdistline(gca);
api = iptgetapi(h);
fcn = makeConstrainToRectFcn('imline',...
                              get(gca,'XLim'),get(gca,'YLim'));
api.setDragConstraintFcn(fcn);


% --------------------------------------------------------------------
function Ellipse_Callback(hObject, eventdata, handles)
% hObject    handle to Ellipse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Rectangle_Callback(hObject, eventdata, handles)
% hObject    handle to Rectangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_24_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Hide_Area_Callback(hObject, eventdata, handles)
% hObject    handle to Hide_Area (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Delete_All_Callback(hObject, eventdata, handles)
% hObject    handle to Delete_All (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Filters_Callback(hObject, eventdata, handles)
% hObject    handle to Filters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Flip_and_Rotate_Callback(hObject, eventdata, handles)
% hObject    handle to Flip_and_Rotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Default_Callback(hObject, eventdata, handles)
% hObject    handle to Default (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Open_File_Callback(hObject, eventdata, handles)
% hObject    handle to Open_File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.o = 3 ;
[filename, pathname] = uigetfile({'*.dcm'},'File Selector');
handles.pathname = pathname;
handles.filedcm = strcat(pathname, filename);
X = dicomread(handles.filedcm);
handles.dicom_image = X;
guidata(hObject, handles);
axes(handles.axes1);
imshow(X,[]);
info = dicominfo(handles.filedcm);
set(handles.Name,'string', info.PatientName.FamilyName);
set(handles.ID, 'string', info.PatientID);
if(isfield(info,'PatientBirthDate'))
set(handles.Birth, 'string', info.PatientBirthDate);
end
if(isfield(info,'PatientSex'))
set(handles.Sex, 'string', info.PatientSex);
end
if(isfield(info,'PatientAge'))
set(handles.Age, 'string', info.PatientAge);
end
if(isfield(info,'PatientWeight'))
set(handles.Weight, 'string', info.PatientWeight);
end
if(isfield(info,'PatientAddress'))
set(handles.Address, 'string', info.PatientAddress);
end
if(isfield(info,'StudyDate'))
set(handles.StudyDate, 'string', info.StudyDate);
end
if(isfield(info,'StudyTime'))
set(handles.StudyTime, 'string', info.StudyTime);
end
if(isfield(info,'StudyID'))
set(handles.StudyID, 'string', info.StudyID);
end
if(isfield(info,'Modality'))
set(handles.StudyModality, 'string', info.Modality);
end
if(isfield(info,'StudyDescription'))
set(handles.Study, 'string', info.StudyDescription);
end
if(isfield(info,'SeriesDate'))
set(handles.SeriesDate, 'string', info.SeriesDate);
end
if(isfield(info,'SeriesTime'))
set(handles.SeriesTime, 'string', info.SeriesTime);
end
if(isfield(info,'SeriesDescription'))
set(handles.Series, 'string', info.SeriesDescription);
end
set(handles.text_patientName, 'enable' ,'on' );
set(handles.text_patientID, 'enable' ,'on' );
set(handles.text_patientBirthDate, 'enable' ,'on');
set(handles.text_patientSex, 'enable' ,'on' );
set(handles.text_patientAge, 'enable' ,'on');
set(handles.text_patientWeight, 'enable' ,'on' );
set(handles.text_patientAddress, 'enable' ,'on' );
set(handles.text_studyDate, 'enable' ,'on' );
set(handles.text_studyTime, 'enable' ,'on' );
set(handles.text_studyID, 'enable' ,'on' );
set(handles.text_studyModality, 'enable' ,'on' );
set(handles.text_studyDescription, 'enable' ,'on' );
set(handles.text_seriesDate, 'enable' ,'on' );
set(handles.text_seriesTime, 'enable' ,'on' );
set(handles.text_seriesDescription, 'enable' ,'on' );
axes(handles.axes1);
set(gca,'clim',[0 handles.x]);
% --------------------------------------------------------------------
function Export_To_Callback(hObject, eventdata, handles)
% hObject    handle to Export_To (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Export_to_PNG_Callback(hObject, eventdata, handles)
% hObject    handle to Export_to_PNG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
F = getframe(gca);
imwrite(F.cdata, 'File.png');
imshow('File.png');
imsave
myicon = imread('icon.png');
msgbox('Save Complete', 'Success', 'custom', myicon);

% --------------------------------------------------------------------
function Export_to_JPG_Callback(hObject, eventdata, handles)
% hObject    handle to Export_to_JPG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
F = getframe(gca);
imwrite(F.cdata, 'File.jpg');
imshow('File.jpg');
imsave
myicon = imread('icon.png');
msgbox('Save Complete', 'Success', 'custom', myicon);

% --------------------------------------------------------------------
function Export_to_GIF_Callback(hObject, eventdata, handles)
% hObject    handle to Export_to_GIF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
F = getframe(gca);
imwrite(F.cdata, 'File.gif');
imshow('File.gif');
imsave
myicon = imread('icon.png');
msgbox('Save Complete', 'Success', 'custom', myicon);


% --------------------------------------------------------------------
function Export_to_BMP_Callback(hObject, eventdata, handles)
% hObject    handle to Export_to_BMP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
F = getframe(gca);
imwrite(F.cdata, 'File.bmp');
imshow('File.bmp');
imsave
myicon = imread('icon.png');
msgbox('Save Complete', 'Success', 'custom', myicon);

% --------------------------------------------------------------------
function Flip_Horizontal_Callback(hObject, eventdata, handles)
% hObject    handle to Flip_Horizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = flip(I,2);
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

% --------------------------------------------------------------------
function Flip_Vertical_Callback(hObject, eventdata, handles)
% hObject    handle to Flip_Vertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = flip(I,1);
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

% --------------------------------------------------------------------
function Rotate_90_Left_Callback(hObject, eventdata, handles)
% hObject    handle to Rotate_90_Left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = imrotate(I,90,'bilinear');
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

% --------------------------------------------------------------------
function Rotate_90_Right_Callback(hObject, eventdata, handles)
% hObject    handle to Rotate_90_Right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = imrotate(I,-90,'bilinear');
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

% --------------------------------------------------------------------
function Black_and_White_Callback(hObject, eventdata, handles)
% hObject    handle to Black_and_White (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
P = imbinarize(handles.dicom_image, graythresh(handles.dicom_image)); % convert to black and while ladder
imshow(P, []);

% --------------------------------------------------------------------
function Increase_Border_Brightness_Callback(hObject, eventdata, handles)
% hObject    handle to Increase_Border_Brightness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dicom_image = handles.dicom_image;
[r,c]= size(dicom_image);
k= fspecial('gaussian',[3 3],1); %bo loc gauss
f= imfilter(dicom_image,k); %loc anh voi bo loc gauss
border = dicom_image-f; %lay duong vien
min_border= min(min(border));
max_border= max(max(border));
mid1= abs(min_border+max_border)/2;
[r,c]= size(border);
g1= 7;%he so nhan
g2= 0.4;
for i= 1:r
    for j= 1:c
        if abs(border(i,j))< mid1
            border(i,j)= g1*border(i,j);
        else if abs(border(i,j))>= mid1
                border(i,j)= g2*border(i,j);
            end
        end
    end
end
border= uint8(border);
axes(handles.axes1);
imshow(border,[]);

% --------------------------------------------------------------------
function Deleted_White_Dot_Callback(hObject, eventdata, handles)
% hObject    handle to Deleted_White_Dot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Pb = imbinarize(handles.dicom_image, graythresh(handles.dicom_image));
P = bwareaopen(Pb, 250);
imshow(P, []);

% --------------------------------------------------------------------
function Filled_Hole_Callback(hObject, eventdata, handles)
% hObject    handle to Filled_Hole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
P = handles.dicom_image;
Pb = imbinarize(handles.dicom_image, graythresh(handles.dicom_image));
Pb = bwareaopen(Pb, 250);
Pb = imfill(Pb, 'holes'); 
P(~Pb) = 0;
imshow(P, []);

% --------------------------------------------------------------------
function Chose_White_Spot_Callback(hObject, eventdata, handles)
% hObject    handle to Chose_White_Spot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
P = handles.dicom_image;
Pb = imbinarize(handles.dicom_image, graythresh(handles.dicom_image));
Pb = bwareaopen(Pb, 250);
Pb = imfill(Pb, 'holes'); 
P(~Pb) = 0;
props = regionprops(Pb,'Area','PixelIdxList'); 
[m,Index] = max([props.Area]);
Pbm = zeros(size(P,1),size(P,2));
Pbm(props(Index).PixelIdxList)=1;
P(~Pbm)=0;
clear props
clear Index
clear max
imshow(P, []);

% --------------------------------------------------------------------
function View_Callback(hObject, eventdata, handles)
% hObject    handle to View (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Zoom_Callback(hObject, eventdata, handles)
% hObject    handle to Zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
zoom on;

% --------------------------------------------------------------------
function V3D_Callback(hObject, eventdata, handles)
% hObject    handle to V3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Frame_Callback(hObject, eventdata, handles)
% hObject    handle to Frame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Single_Image_Callback(hObject, eventdata, handles)
% hObject    handle to Single_Image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dicom_image = dicomread(handles.filedcm);
imshow(dicom_image,[]);
handles.dicom_image = dicom_image;
guidata(hObject, handles);
zoom off;

% --------------------------------------------------------------------
function Multiple_Images_Callback(hObject, eventdata, handles)
% hObject    handle to Multiple_Images (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fileNamess = {handles.filenames.name};
I = dicomread(fullfile(handles.image_folder,fileNamess{1}));
info = dicominfo(fullfile(handles.image_folder,fileNamess{1}));
numImages = length(fileNamess);
classI  = class(I);
sizeI   = size(I);
mri = zeros(info.Rows,info.Columns,numImages, classI);
for i=length(fileNamess):-1:1
    fname = fullfile(handles.image_folder,fileNamess{i});
    mri(:,:,i)=uint16(dicomread(fname));
    waitbar((length(fileNamess)-i+1)/length(fileNamess));
end
axes(handles.axes1);
montage(reshape(uint16(mri),[size(mri,1),size(mri,2),1,size(mri,3)]),'DisplayRange',[]);
set(gca,'clim',[0 handles.x]);
% set(handles.pushbutton_Zoom, 'enable' ,'off' );
% set(handles.pushbutton_3d, 'enable' ,'off' );
% set(handles.pushbutton_Flip_Horizontal, 'enable' ,'off' );
% set(handles.pushbutton_Flip_Vertical, 'enable' ,'off' );

% --------------------------------------------------------------------
function Image_3D_Callback(hObject, eventdata, handles)
% hObject    handle to Image_3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Image_2D_Callback(hObject, eventdata, handles)
% hObject    handle to Image_2D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Export_to_PNG_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Export_to_PNG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_openFile.
function pushbutton_openFile_Callback(hObject, eventdata, handles)
handles.o = 3 ;
[filename, pathname] = uigetfile({'*.dcm'},'File Selector');
handles.pathname = pathname;
handles.filedcm = strcat(pathname, filename);
X = dicomread(handles.filedcm);
handles.dicom_image = X;
guidata(hObject, handles);
axes(handles.axes1);
imshow(X,[]);
info = dicominfo(handles.filedcm);
set(handles.Name,'string', info.PatientName.FamilyName);
set(handles.ID, 'string', info.PatientID);
if(isfield(info,'PatientBirthDate'))
set(handles.Birth, 'string', info.PatientBirthDate);
end
if(isfield(info,'PatientSex'))
set(handles.Sex, 'string', info.PatientSex);
end
if(isfield(info,'PatientAge'))
set(handles.Age, 'string', info.PatientAge);
end
if(isfield(info,'PatientWeight'))
set(handles.Weight, 'string', info.PatientWeight);
end
if(isfield(info,'PatientAddress'))
set(handles.Address, 'string', info.PatientAddress);
end
if(isfield(info,'StudyDate'))
set(handles.StudyDate, 'string', info.StudyDate);
end
if(isfield(info,'StudyTime'))
set(handles.StudyTime, 'string', info.StudyTime);
end
if(isfield(info,'StudyID'))
set(handles.StudyID, 'string', info.StudyID);
end
if(isfield(info,'Modality'))
set(handles.StudyModality, 'string', info.Modality);
end
if(isfield(info,'StudyDescription'))
set(handles.Study, 'string', info.StudyDescription);
end
if(isfield(info,'SeriesDate'))
set(handles.SeriesDate, 'string', info.SeriesDate);
end
if(isfield(info,'SeriesTime'))
set(handles.SeriesTime, 'string', info.SeriesTime);
end
if(isfield(info,'SeriesDescription'))
set(handles.Series, 'string', info.SeriesDescription);
end
set(handles.text_patientName, 'enable' ,'on' );
set(handles.text_patientID, 'enable' ,'on' );
set(handles.text_patientBirthDate, 'enable' ,'on');
set(handles.text_patientSex, 'enable' ,'on' );
set(handles.text_patientAge, 'enable' ,'on');
set(handles.text_patientWeight, 'enable' ,'on' );
set(handles.text_patientAddress, 'enable' ,'on' );
set(handles.text_studyDate, 'enable' ,'on' );
set(handles.text_studyTime, 'enable' ,'on' );
set(handles.text_studyID, 'enable' ,'on' );
set(handles.text_studyModality, 'enable' ,'on' );
set(handles.text_studyDescription, 'enable' ,'on' );
set(handles.text_seriesDate, 'enable' ,'on' );
set(handles.text_seriesTime, 'enable' ,'on' );
set(handles.text_seriesDescription, 'enable' ,'on' );
axes(handles.axes1);
set(gca,'clim',[0 handles.x]);
% hObject    handle to pushbutton_openFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_exportToPNG.
function pushbutton_exportToPNG_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_exportToPNG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
F = getframe(gca);
imwrite(F.cdata, 'File.png');
imshow('File.png');
imsave
myicon = imread('icon.png');
msgbox('Save Complete', 'Success', 'custom', myicon);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_Zoom.
function pushbutton_Zoom_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
zoom on;

% --- Executes on button press in pushbutton_3d.
function pushbutton_3d_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_3d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_Flip_Horizontal.
function pushbutton_Flip_Horizontal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Flip_Horizontal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = flip(I,2);
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

% --- Executes on button press in pushbutton_Flip_Vertical.
function pushbutton_Flip_Vertical_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Flip_Vertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = flip(I,1);
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

function Name_Callback(hObject, eventdata, handles)
% hObject    handle to Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Name as text
%        str2double(get(hObject,'String')) returns contents of Name as a double


% --- Executes during object creation, after setting all properties.
function Name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Birth_Callback(hObject, eventdata, handles)
% hObject    handle to Birth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Birth as text
%        str2double(get(hObject,'String')) returns contents of Birth as a double


% --- Executes during object creation, after setting all properties.
function Birth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Birth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Sex_Callback(hObject, eventdata, handles)
% hObject    handle to Sex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Sex as text
%        str2double(get(hObject,'String')) returns contents of Sex as a double


% --- Executes during object creation, after setting all properties.
function Sex_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Age_Callback(hObject, eventdata, handles)
% hObject    handle to Age (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Age as text
%        str2double(get(hObject,'String')) returns contents of Age as a double


% --- Executes during object creation, after setting all properties.
function Age_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Age (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Weight_Callback(hObject, eventdata, handles)
% hObject    handle to Weight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Weight as text
%        str2double(get(hObject,'String')) returns contents of Weight as a double


% --- Executes during object creation, after setting all properties.
function Weight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Weight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Address_Callback(hObject, eventdata, handles)
% hObject    handle to Address (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Address as text
%        str2double(get(hObject,'String')) returns contents of Address as a double


% --- Executes during object creation, after setting all properties.
function Address_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Address (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StudyDate_Callback(hObject, eventdata, handles)
% hObject    handle to StudyDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StudyDate as text
%        str2double(get(hObject,'String')) returns contents of StudyDate as a double


% --- Executes during object creation, after setting all properties.
function StudyDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StudyDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StudyTime_Callback(hObject, eventdata, handles)
% hObject    handle to StudyTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StudyTime as text
%        str2double(get(hObject,'String')) returns contents of StudyTime as a double


% --- Executes during object creation, after setting all properties.
function StudyTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StudyTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StudyID_Callback(hObject, eventdata, handles)
% hObject    handle to StudyID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StudyID as text
%        str2double(get(hObject,'String')) returns contents of StudyID as a double


% --- Executes during object creation, after setting all properties.
function StudyID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StudyID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StudyModality_Callback(hObject, eventdata, handles)
% hObject    handle to StudyModality (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StudyModality as text
%        str2double(get(hObject,'String')) returns contents of StudyModality as a double


% --- Executes during object creation, after setting all properties.
function StudyModality_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StudyModality (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Study_Callback(hObject, eventdata, handles)
% hObject    handle to Study (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Study as text
%        str2double(get(hObject,'String')) returns contents of Study as a double


% --- Executes during object creation, after setting all properties.
function Study_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Study (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SeriesDate_Callback(hObject, eventdata, handles)
% hObject    handle to SeriesDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SeriesDate as text
%        str2double(get(hObject,'String')) returns contents of SeriesDate as a double


% --- Executes during object creation, after setting all properties.
function SeriesDate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SeriesDate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SeriesTime_Callback(hObject, eventdata, handles)
% hObject    handle to SeriesTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SeriesTime as text
%        str2double(get(hObject,'String')) returns contents of SeriesTime as a double


% --- Executes during object creation, after setting all properties.
function SeriesTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SeriesTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Series_Callback(hObject, eventdata, handles)
% hObject    handle to Series (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Series as text
%        str2double(get(hObject,'String')) returns contents of Series as a double


% --- Executes during object creation, after setting all properties.
function Series_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Series (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ID_Callback(hObject, eventdata, handles)
% hObject    handle to ID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ID as text
%        str2double(get(hObject,'String')) returns contents of ID as a double


% --- Executes during object creation, after setting all properties.
function ID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on pushbutton_Zoom and none of its controls.
function pushbutton_Zoom_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton_Zoom (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Open_Folder_Callback(hObject, eventdata, handles)
% hObject    handle to Open_Folder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.image_folder = uigetdir;
handles.filenames = dir(fullfile(handles.image_folder, '*.dcm')); 
handles.soanh = numel(handles.filenames);
f= fullfile(handles.image_folder, handles.filenames(handles.n).name);

files = handles.filenames;
% Loop on every element in the folder and update the list
MyListOfFiles = [];
for i = 1:length(files)
   if files(i).isdir==0
       MyListOfFiles{end+1,1} = files(i).name;
   end
end
% update the listbox with the result
set(handles.listbox1,'String',MyListOfFiles)
handles.MyListOfFiles = MyListOfFiles;
guidata(hObject, handles);
X= dicomread(f);
handles.dicom_image = X;
axes(handles.axes1);
imshow(X,[]);
info = dicominfo(f);
set(handles.Name,'string', info.PatientName.FamilyName);
set(handles.ID, 'string', info.PatientID);
if(isfield(info,'PatientBirthDate'))
set(handles.Birth, 'string', info.PatientBirthDate);
end
if(isfield(info,'PatientSex'))
set(handles.Sex, 'string', info.PatientSex);
end
if(isfield(info,'PatientAge'))
set(handles.Age, 'string', info.PatientAge);
end
if(isfield(info,'PatientWeight'))
set(handles.Weight, 'string', info.PatientWeight);
end
if(isfield(info,'PatientAddress'))
set(handles.Address, 'string', info.PatientAddress);
end
if(isfield(info,'StudyDate'))
set(handles.StudyDate, 'string', info.StudyDate);
end
if(isfield(info,'StudyTime'))
set(handles.StudyTime, 'string', info.StudyTime);
end
if(isfield(info,'StudyID'))
set(handles.StudyID, 'string', info.StudyID);
end
if(isfield(info,'Modality'))
set(handles.StudyModality, 'string', info.Modality);
end
if(isfield(info,'StudyDescription'))
set(handles.Study, 'string', info.StudyDescription);
end
if(isfield(info,'SeriesDate'))
set(handles.SeriesDate, 'string', info.SeriesDate);
end
if(isfield(info,'SeriesTime'))
set(handles.SeriesTime, 'string', info.SeriesTime);
end
if(isfield(info,'SeriesDescription'))
set(handles.Series, 'string', info.SeriesDescription);
end
set(handles.text_patientName, 'enable' ,'on' );
set(handles.text_patientID, 'enable' ,'on' );
set(handles.text_patientBirthDate, 'enable' ,'on');
set(handles.text_patientSex, 'enable' ,'on' );
set(handles.text_patientAge, 'enable' ,'on');
set(handles.text_patientWeight, 'enable' ,'on' );
set(handles.text_patientAddress, 'enable' ,'on' );
set(handles.text_studyDate, 'enable' ,'on' );
set(handles.text_studyTime, 'enable' ,'on' );
set(handles.text_studyID, 'enable' ,'on' );
set(handles.text_studyModality, 'enable' ,'on' );
set(handles.text_studyDescription, 'enable' ,'on' );
set(handles.text_seriesDate, 'enable' ,'on' );
set(handles.text_seriesTime, 'enable' ,'on' );
set(handles.text_seriesDescription, 'enable' ,'on' );
axes(handles.axes1);
set(gca,'clim',[0 handles.x]);


% --- Executes on slider movement.
function slider_contrast_Callback(hObject, eventdata, handles)
% hObject    handle to slider_contrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
axes(handles.axes1);
imshow(handles.dicom_image,[]);
y=get(handles.slider_contrast,'Value');
handles.x=6000-5999*y ;
guidata(hObject, handles);
set(gca,'clim',[0 handles.x]);

% --- Executes during object creation, after setting all properties.
function slider_contrast_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_contrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_brightness_Callback(hObject, eventdata, handles)
% hObject    handle to slider_brightness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
axes(handles.axes1);
imshow(handles.dicom_image,[]);
demon = handles.dicom_image + 29600 +3100*get(handles.slider_brightness,'Value');
imshow(demon,[]);

% --- Executes during object creation, after setting all properties.
function slider_brightness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_brightness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
listbox = get(handles.listbox1, 'string');
selected = get(handles.listbox1, 'value');
selected_image_name = listbox(selected);
handles.filedcm = strcat(handles.image_folder,'\', selected_image_name);
handles.filedcm = char(handles.filedcm)
X = dicomread(handles.filedcm);
handles.dicom_image = X;
guidata(hObject, handles);
axes(handles.axes1);
imshow(X,[]);

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_playMultiple.
function button_playMultiple_Callback(hObject, eventdata, handles)
% hObject    handle to button_playMultiple (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fileNames = handles.MyListOfFiles;
folder_name = handles.image_folder;
fileNames
folder_name
for i = 1:length(fileNames)
  %extract size info from metadata
  set(handles.listbox1, 'value', i);
 info = dicominfo(fullfile(folder_name,fileNames{i}));
 handles.info_dicom=info;
     guidata(hObject,handles);
axes(handles.axes1);
demo=dicomread(fullfile(folder_name,fileNames{i}));
imshow(demo,[]);
end;
% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in button_back.
function button_back_Callback(hObject, eventdata, handles)
% hObject    handle to button_back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.n = handles.n - 1; 
handles.filenames = dir(fullfile(handles.image_folder, '*.dcm'));
if (handles.n < 1 ) 
    handles.n = handles.soanh ; 
end
f = fullfile(handles.image_folder, handles.filenames(handles.n).name);        
X = dicomread(f); 
axes(handles.axes1);
imshow(X,[]);
guidata(hObject, handles);
info = dicominfo(f);
set(handles.Name,'string', info.PatientName.FamilyName);
set(handles.ID, 'string', info.PatientID);
if(isfield(info,'PatientBirthDate'))
set(handles.Birth, 'string', info.PatientBirthDate);
end
if(isfield(info,'PatientSex'))
set(handles.Sex, 'string', info.PatientSex);
end
if(isfield(info,'PatientAge'))
set(handles.Age, 'string', info.PatientAge);
end
if(isfield(info,'PatientWeight'))
set(handles.Weight, 'string', info.PatientWeight);
end
if(isfield(info,'PatientAddress'))
set(handles.Address, 'string', info.PatientAddress);
end
if(isfield(info,'StudyDate'))
set(handles.StudyDate, 'string', info.StudyDate);
end
if(isfield(info,'StudyTime'))
set(handles.StudyTime, 'string', info.StudyTime);
end
if(isfield(info,'StudyID'))
set(handles.StudyID, 'string', info.StudyID);
end
if(isfield(info,'Modality'))
set(handles.StudyModality, 'string', info.Modality);
end
if(isfield(info,'StudyDescription'))
set(handles.Study, 'string', info.StudyDescription);
end
if(isfield(info,'SeriesDate'))
set(handles.SeriesDate, 'string', info.SeriesDate);
end
if(isfield(info,'SeriesTime'))
set(handles.SeriesTime, 'string', info.SeriesTime);
end
if(isfield(info,'SeriesDescription'))
set(handles.Series, 'string', info.SeriesDescription);
end
set(handles.text_patientName, 'enable' ,'on' );
set(handles.text_patientID, 'enable' ,'on' );
set(handles.text_patientBirthDate, 'enable' ,'on');
set(handles.text_patientSex, 'enable' ,'on' );
set(handles.text_patientAge, 'enable' ,'on');
set(handles.text_patientWeight, 'enable' ,'on' );
set(handles.text_patientAddress, 'enable' ,'on' );
set(handles.text_studyDate, 'enable' ,'on' );
set(handles.text_studyTime, 'enable' ,'on' );
set(handles.text_studyID, 'enable' ,'on' );
set(handles.text_studyModality, 'enable' ,'on' );
set(handles.text_studyDescription, 'enable' ,'on' );
set(handles.text_seriesDate, 'enable' ,'on' );
set(handles.text_seriesTime, 'enable' ,'on' );
axes(handles.axes1);
set(gca,'clim',[0 handles.x]);

% --- Executes on button press in button_next.
function button_next_Callback(hObject, eventdata, handles)
% hObject    handle to button_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.n = handles.n + 1;
handles.filenames = dir(fullfile(handles.image_folder, '*.dcm'));
if (handles.n > handles.soanh ) 
    handles.n=1 ; 
end
f = fullfile(handles.image_folder, handles.filenames(handles.n).name);
X= dicomread(f);
handles.dicom_image = X;
axes(handles.axes1);
imshow(X,[]);
guidata(hObject, handles);
info = dicominfo(f);
set(handles.Name,'string', info.PatientName.FamilyName);
set(handles.ID, 'string', info.PatientID);
if(isfield(info,'PatientBirthDate'))
set(handles.Birth, 'string', info.PatientBirthDate);
end
if(isfield(info,'PatientSex'))
set(handles.Sex, 'string', info.PatientSex);
end
if(isfield(info,'PatientAge'))
set(handles.Age, 'string', info.PatientAge);
end
if(isfield(info,'PatientWeight'))
set(handles.Weight, 'string', info.PatientWeight);
end
if(isfield(info,'PatientAddress'))
set(handles.Address, 'string', info.PatientAddress);
end
if(isfield(info,'StudyDate'))
set(handles.StudyDate, 'string', info.StudyDate);
end
if(isfield(info,'StudyTime'))
set(handles.StudyTime, 'string', info.StudyTime);
end
if(isfield(info,'StudyID'))
set(handles.StudyID, 'string', info.StudyID);
end
if(isfield(info,'Modality'))
set(handles.StudyModality, 'string', info.Modality);
end
if(isfield(info,'StudyDescription'))
set(handles.Study, 'string', info.StudyDescription);
end
if(isfield(info,'SeriesDate'))
set(handles.SeriesDate, 'string', info.SeriesDate);
end
if(isfield(info,'SeriesTime'))
set(handles.SeriesTime, 'string', info.SeriesTime);
end
if(isfield(info,'SeriesDescription'))
set(handles.Series, 'string', info.SeriesDescription);
end
set(handles.text_patientName, 'enable' ,'on' );
set(handles.text_patientID, 'enable' ,'on' );
set(handles.text_patientBirthDate, 'enable' ,'on');
set(handles.text_patientSex, 'enable' ,'on' );
set(handles.text_patientAge, 'enable' ,'on');
set(handles.text_patientWeight, 'enable' ,'on' );
set(handles.text_patientAddress, 'enable' ,'on' );
set(handles.text_studyDate, 'enable' ,'on' );
set(handles.text_studyTime, 'enable' ,'on' );
set(handles.text_studyID, 'enable' ,'on' );
set(handles.text_studyModality, 'enable' ,'on' );
set(handles.text_studyDescription, 'enable' ,'on' );
set(handles.text_seriesDate, 'enable' ,'on' );
set(handles.text_seriesTime, 'enable' ,'on' );
set(handles.text_seriesDescription, 'enable' ,'on' );
axes(handles.axes1);
set(gca,'clim',[0 handles.x]);


% --- Executes on button press in pushbutton_default.
function pushbutton_default_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_default (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dicom_image = dicomread(handles.filedcm);
imshow(dicom_image,[]);
handles.dicom_image = dicom_image;
guidata(hObject, handles);
zoom off;


% --- Executes on button press in pushbutton_rotateRight.
function pushbutton_rotateRight_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_rotateRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = imrotate(I,-90,'bilinear');
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);

% --- Executes on button press in pushbutton_rotateLeft.
function pushbutton_rotateLeft_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_rotateLeft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = handles.dicom_image;
y = imrotate(I,90,'bilinear');
handles.dicom_image = y;
guidata(hObject, handles);
imshow(y, []);


% --- Executes on button press in pushbutton_openFolder.
function pushbutton_openFolder_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_openFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.image_folder = uigetdir;
handles.filenames = dir(fullfile(handles.image_folder, '*.dcm')); 
handles.soanh = numel(handles.filenames);
f= fullfile(handles.image_folder, handles.filenames(handles.n).name);

files = handles.filenames;
% Loop on every element in the folder and update the list
MyListOfFiles = [];
for i = 1:length(files)
   if files(i).isdir==0
       MyListOfFiles{end+1,1} = files(i).name;
   end
end
% update the listbox with the result
set(handles.listbox1,'String',MyListOfFiles)
handles.MyListOfFiles = MyListOfFiles;
guidata(hObject, handles);
X= dicomread(f);
handles.dicom_image = X;
axes(handles.axes1);
imshow(X,[]);
info = dicominfo(f);
set(handles.Name,'string', info.PatientName.FamilyName);
set(handles.ID, 'string', info.PatientID);
if(isfield(info,'PatientBirthDate'))
set(handles.Birth, 'string', info.PatientBirthDate);
end
if(isfield(info,'PatientSex'))
set(handles.Sex, 'string', info.PatientSex);
end
if(isfield(info,'PatientAge'))
set(handles.Age, 'string', info.PatientAge);
end
if(isfield(info,'PatientWeight'))
set(handles.Weight, 'string', info.PatientWeight);
end
if(isfield(info,'PatientAddress'))
set(handles.Address, 'string', info.PatientAddress);
end
if(isfield(info,'StudyDate'))
set(handles.StudyDate, 'string', info.StudyDate);
end
if(isfield(info,'StudyTime'))
set(handles.StudyTime, 'string', info.StudyTime);
end
if(isfield(info,'StudyID'))
set(handles.StudyID, 'string', info.StudyID);
end
if(isfield(info,'Modality'))
set(handles.StudyModality, 'string', info.Modality);
end
if(isfield(info,'StudyDescription'))
set(handles.Study, 'string', info.StudyDescription);
end
if(isfield(info,'SeriesDate'))
set(handles.SeriesDate, 'string', info.SeriesDate);
end
if(isfield(info,'SeriesTime'))
set(handles.SeriesTime, 'string', info.SeriesTime);
end
if(isfield(info,'SeriesDescription'))
set(handles.Series, 'string', info.SeriesDescription);
end
set(handles.text_patientName, 'enable' ,'on' );
set(handles.text_patientID, 'enable' ,'on' );
set(handles.text_patientBirthDate, 'enable' ,'on');
set(handles.text_patientSex, 'enable' ,'on' );
set(handles.text_patientAge, 'enable' ,'on');
set(handles.text_patientWeight, 'enable' ,'on' );
set(handles.text_patientAddress, 'enable' ,'on' );
set(handles.text_studyDate, 'enable' ,'on' );
set(handles.text_studyTime, 'enable' ,'on' );
set(handles.text_studyID, 'enable' ,'on' );
set(handles.text_studyModality, 'enable' ,'on' );
set(handles.text_studyDescription, 'enable' ,'on' );
set(handles.text_seriesDate, 'enable' ,'on' );
set(handles.text_seriesTime, 'enable' ,'on' );
set(handles.text_seriesDescription, 'enable' ,'on' );
axes(handles.axes1);
set(gca,'clim',[0 handles.x]);


% --- Executes on button press in pushbutton_distanceCalculate.
function pushbutton_distanceCalculate_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_distanceCalculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1)
h = imdistline(gca);
api = iptgetapi(h);
fcn = makeConstrainToRectFcn('imline',...
                              get(gca,'XLim'),get(gca,'YLim'));
api.setDragConstraintFcn(fcn);


% --- Executes on button press in pushbutton_multipleImages.
function pushbutton_multipleImages_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_multipleImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fileNamess = {handles.filenames.name};
I = dicomread(fullfile(handles.image_folder,fileNamess{1}));
info = dicominfo(fullfile(handles.image_folder,fileNamess{1}));
numImages = length(fileNamess);
classI  = class(I);
sizeI   = size(I);
mri = zeros(info.Rows,info.Columns,numImages, classI);
for i=length(fileNamess):-1:1
    fname = fullfile(handles.image_folder,fileNamess{i});
    mri(:,:,i)=uint16(dicomread(fname));
    waitbar((length(fileNamess)-i+1)/length(fileNamess));
end
axes(handles.axes1);
montage(reshape(uint16(mri),[size(mri,1),size(mri,2),1,size(mri,3)]),'DisplayRange',[]);
set(gca,'clim',[0 handles.x]);


% --- Executes when DICOM_Tags is resized.
function DICOM_Tags_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to DICOM_Tags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function DICOM_Tags_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to DICOM_Tags (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
